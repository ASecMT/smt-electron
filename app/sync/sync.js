/**
 * Este módulo esta diseñado para correr en el renderer
 * tiene acceso a las apis: nodejs (via preloadNode.js), chromium 
 */

const req = typeof require == "undefined" ? _require : require;
const Logued = req('../storage/Logued.js');

const grupos = ($, grupos, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarGrupos', { Grupos: grupos }, res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.grupos = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
}); 

const alumnos = ($, alumnos, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarAlumnos', { alumnos }, res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.alumnos = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const sesiones = ($, sesiones, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarAsistencia', { sesiones }, res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.sesiones = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const trabajos = ($, trabajos, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarTrabajos', { trabajos }, res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.trabajos = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const examenes = ($, examenes, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarExamenes', { examenes }, res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.examenes = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const instrumentos = ($, instrumentos, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarInstrumentos', { instrumentos }, res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.instrumentos = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const habilidades = ($, habilidades, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarHabilidades', { habilidades }, res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.habilidades = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const reporte = ($, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarReporte', res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.reporte = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const diagnosticoCiclo = ($, diagnosticoCiclo, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarDiagnosticoCiclo', { diagnosticoCiclo }, res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.diagnosticoCiclo = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const tipoPortafolio = ($, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarTipoPortafolio', res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.tipoPortafolio = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const semaforo = ($, server) => new Promise((ok, no) => {
  $.post(server + '/DesktopApi/SincronizarSemaforo', res => {
    if (res.result)
      Logued.get().then(smtdb => {
        smtdb.semaforo = res.data;
        Logued.set(smtdb);

        ok(smtdb);
      }).catch(() => no({ message: "Ocurrió un problema al sincronizar" }));
    else no(res.message);
  }).error(err => no({ message: "Ocurrió un problema al sincronizar" }));
});

const all = ($, smtdb, server) => {
    const grupos = smtdb.grupos.filter(g => g.hasOwnProperty("FechaSync"));
    const alumnos = smtdb.alumnos.filter(g => g.hasOwnProperty("FechaSync"));
    const habilidades = smtdb.habilidades.filter(g => g.hasOwnProperty("FechaSync"));
    
    const diagnosticoCiclo = smtdb.diagnosticoCiclo
        .filter(g => 
          g.hasOwnProperty("FechaSync") || 
          g.Temas.filter(t => 
              t.hasOwnProperty("FechaSync") || 
              t.Alumnos.filter(a => 
                  a.hasOwnProperty("FechaSync")).length).length)
        .map(g => {
          g.Temas.map(t => {
              delete t.tds;
              return t;
          })
          return g
        })

    const examenes = smtdb.examenes
      .filter(g => 
          g.hasOwnProperty("FechaSync") || 
          g.Temas.filter(t => 
              t.hasOwnProperty("FechaSync") || 
              t.Alumnos.filter(a => 
                  a.hasOwnProperty("FechaSync")).length).length)
      .map(g => {
          g.Temas.map(t => {
              delete t.tds;
              return t;
          })
          return g
      })

    const instrumentos = smtdb.instrumentos
      .filter(g => g.hasOwnProperty("FechaSync") || g.entrega.filter(a => a.hasOwnProperty("FechaSync")).length)
      .map(g => { g.entrega = (g.entrega || []).filter(a => a.hasOwnProperty("FechaSync")); return g });

    const trabajos = smtdb.trabajos
      .filter(g => g.hasOwnProperty("FechaSync") || g.entrega.filter(a => a.hasOwnProperty("FechaSync")).length)
      .map(g => { g.entrega = (g.entrega || []).filter(a => a.hasOwnProperty("FechaSync")); return g });

    const sesiones = smtdb.sesiones
      .filter(g => g.hasOwnProperty("FechaSync") || g.asistencia.filter(a => a.hasOwnProperty("FechaSync")).length)
      .map(g => {
        g.asistencia = (g.asistencia || []).filter(a => a.hasOwnProperty("FechaSync"));
        delete g.tds; 
        return g;
      });

    return _Sync.grupos($, grupos, server)
        .then(() => _Sync.alumnos($, alumnos, server))
        .then(() => _Sync.sesiones($, sesiones, server))
        .then(() => _Sync.trabajos($, trabajos, server))
        .then(() => _Sync.examenes($, examenes, server))
        .then(() => _Sync.instrumentos($, instrumentos, server))
        .then(() => _Sync.habilidades($, habilidades, server))
        .then(() => _Sync.diagnosticoCiclo($, diagnosticoCiclo, server))
        .then(() => _Sync.tipoPortafolio($, server))
        .then(() => _Sync.semaforo($, server))
        .then(() => _Sync.reporte($, server))
};

module.exports = {
  grupos, alumnos, sesiones, trabajos,
  examenes, instrumentos, habilidades, 
  reporte, diagnosticoCiclo, tipoPortafolio,
  semaforo, all
};