var acumulador=1;
function Examen () {
    var _a = this;
    var tdsCaptura = '',
        rowNumeros = '',
        rowNombres = '',
        rowCalificacionFinal = '',
        rowAprobados = '',
        rowNoAprobados = '',
        rowExamen = '',
        tdsTotales = '';

    $("#examenes").keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code == '9') {
            e.preventDefault();
            e.stopPropagation();
            var cellIndex = $(e.target).closest('td').index();
            var nxt = $(e.target).closest('tr').next().children().eq(cellIndex);
            if (nxt.data("alumno-id") != undefined) {
                nxt.find("input").focus()
            } else {
                var previo = $(e.target).closest('tr').prev();
                if (previo == undefined || previo.data("total-agregado") != undefined) {
                    $(e.target).closest('td').next().find("input").focus()
                } else {
                    var contador = 0;
                    var c = 0;
                    do {
                        var anterior = $(previo);
                        previo = $(previo).prev();
                        if (previo.data("total-agregado") != undefined) {
                            $(previo).next().children().eq(cellIndex + 1).find("input").focus()
                            contador = 1;
                        } else if (previo.length == 0) {
                            $(anterior).children().eq(cellIndex + 1).find("input").focus()
                            contador = 1;
                        } else if ($(e.target).closest('td').next().find("[data-examen-total]").length > 0) {
                            previo.next().next().next().next().children().eq(7).find("input").focus();
                            contador = 1;
                        }
                        c++;
                        if (c == 10) {
                            contador = 1;
                        }

                    } while (contador == 0);

                }
            }

        }
    })

    var generarNombreCache = function () {
        return 'examen-' + _grupo + '-' + _bimestre;
    }

    this.listar = function () {
        let examenes = __smtdb__.examenes.filter(e => e.IDGrupo == _grupo && e.Bimestre == _bimestre && e.Deleted !== true);
        return Promise.resolve(examenes);

    }

    this.generar = function (selector, examen, focus, autoOrdenar) {
        var elementosExamen = $(selector).find('tbody');
        var regAnterior = undefined;
        if (autoOrdenar == true)
            regAnterior = obtenerPosicion(selector, examen.FechaEntregaDesplegable);

        examen.Temas.forEach(tema => {
            tema.num = 0;
            tema.IDExamen = examen.IDExamen;
            tema.Tipo = examen.Tipo;
            tema.Titulo = examen.Titulo;
            tema.FechaEntregaDesplegable = examen.FechaEntregaDesplegable;
            tema.FechaEntrega = examen.FechaEntrega;
            tema.tds = tdsCaptura.format(tema);

            var element = $(rowExamen(tema)).appendTo(elementosExamen).addData(tema);

            if (regAnterior != undefined)
                $(regAnterior).before(element);
            else
                $(elementosExamen).append(element);

            tema.Alumnos
                .forEach(al => elementosExamen
                    .find(`[data-alumno-id="${al.IDAlumno}"][data-alumno-tema="${tema.IDTema}"] input`)
                        .val(al.Calificacion));

            $(elementosExamen).find('[title]').tooltip();

            if ($('body').hasClass('visualizando'))
                $(elementosExamen).find('input').attr('disabled', true);
        });

        if (focus == true) {
            $('html, body').animate({
                scrollTop: $(selector).find('[data-examen-id="' + examen.IDExamen + '"]:first').offset().top
            }, 1000);

            $(selector).find('[data-examen-id="' + examen.IDExamen + '"]:first').resaltar('info', 3000)
                        .find('input:first')
                        .focus();
        }

    }

    this.tablaExamenTemplate = _require('../app/bimestre/templates/tablaExamen.js');
    this.desplegarResultados = function (grupo, selector) {
        // Se generan rows y tds en base a los alumnos
        // Estos html se generaran cada vez que se ejecute desplegar resultados, 
        // una vez generado ya todas las acciones que requieran estos html solo utilizaran el html que se genero

        rowNombres = '';
        rowNumeros = '';
        tdsCaptura = '';
        rowCalificacionFinal = '';
        rowAprobados = '';
        rowNoAprobados = '';
        tdsTotales = '';

        Alumnos.listar(_grupo).then(() => {
            for (var i = 0; i < Alumnos.data.length; i++) {
                var alumn = Alumnos.data[i];
                rowNumeros += `<th><div class="w100">${i + 1}</div></th>`;
                rowNombres += `<th><div class="w100"><span data-alumno="${alumn.IDAlumno}" class="semaforo" style="background-color:${alumn.semaforo}"></span>${alumn.ApellidoPaterno} ${alumn.ApellidoMaterno} ${alumn.Nombre}</div></th>`;
                rowCalificacionFinal += `<td><div class="w100" data-alumno-final="${alumn.IDAlumno}">0<div></th>`;
                rowAprobados += `<td><div class="w100" data-alumno-aprobado="${alumn.IDAlumno}">0<div></th>`;
                rowNoAprobados += `<td><div class="w100" data-alumno-reprobado="${alumn.IDAlumno}">0<div></th>`;
                tdsTotales += `<td><div class="w100" data-alumno-total="${alumn.IDAlumno}">0<div></th>`;
                tdsCaptura += `<td data-alumno-id="${alumn.IDAlumno}" data-alumno-tema="{IDTema}" ><input name="calificacion" tabindex="0" type="number" min="0" max="100" required="required" class="form-control form-control-oculto w100" value="0" /></td>`;
            }

            let template = _a.tablaExamenTemplate;
            var datos = {
                numerosHeaders: rowNumeros,
                nombresHeaders: rowNombres,
                rowAprobados: rowAprobados,
                rowNoAprobados: rowNoAprobados
            };

            // Se despliega el esqueleto de la asitencias
            $(selector).html(template(datos));

            // Cargar y desplegar datos de sesiones
            _a.listar(grupo).then(function (data) {

                data.map(function (s) {
                    _a.generar(selector, s);
                });

                $(selector).find('.loading').remove();

                setTimeout(function () {
                    // Ligero delay para que se termine de generar las sesiones
                    ajustarRows(selector);
                }, 250);

                Alumnos.cargarSemaforo('examenes');
            });
        });
    }

    var actualizarCalificacion = function (alumno, calificacion, tema,examen) {
        if (calificacion == '' || alumno == '' || tema == undefined) return;
        let input = $('[data-examen-id] [data-alumno-id="' + alumno + '"][data-alumno-tema="' + tema + '"] input');
        let exam = __smtdb__.examenes.filter(e => e.IDExamen == examen);

        if (exam.length) {
            exam = exam[0];
            let tem = exam.Temas.filter(t => t.IDTema == tema);

            if (tem.length) {
                tem = tem[0]
                let almn = tem.Alumnos.filter(a => a.IDAlumno == alumno);
                let creando = !almn.length;

                almn = creando ? {IDAlumno:almn} : almn[0];

                almn["FechaSync"] = (new Date()).toISOString();
                exam["FechaSync"] = (new Date()).toISOString();
                almn["Calificacion"] = ~~calificacion;

                if (creando) tem.Alumnos.push(almn);

                _Logued.set(__smtdb__).then(() => {
                    // Se despliega la sesion actualizada con efecto
                    $('[data-examen-id] [data-alumno-id="' + almn + '"][data-alumno-tema="' + tem + '"]')
                        .html(input)
                        .resaltar('info', 800);

                    actualizarTotales(examen);
                    Alumnos.cargarSemaforo('examenes', alumno);
                })
            }
        }
    }

    var actualizaDataEnCache = function () {
        // Se complica actualizar los datos, asi que se elimina el cache y la proxima vez se volvera a descargar todo
        Cache.vaciar(generarNombreCache());
    }

    var actualizarTotales = function (id) {
        var selector = $('[data-tabla="examen"]');

        // Calcular total de rectivos de un examen
        $(selector).find('[data-total-reactivos="'+id+'"]').each(function () {
            var total = 0;

            $('[data-examen-id="'+ id +'"]').find('[data-reactivos]').each(function () {
                total += parseInt(this.innerHTML);
            });

            this.innerHTML = total;
        });

        // Promedio de trabajos en un tema
        $(selector).find('[data-examen-promedio="' + id + '"]').each(function () {
            var total = $(this).parents('tr').find('[data-alumno-id][data-alumno-tema="' + $(this).parents('tr').attr('data-tema') + '"]').length;
            var reactivos = parseInt($(this).parents('tr').find('[data-reactivos]').text());
            var sumatoria = 0;
            var falsa = 0;

            $(this).parents('tr').find('[data-alumno-id][data-alumno-tema="' + $(this).parents('tr').attr('data-tema') + '"] input').each(function () {
                sumatoria += parseInt($(this).val());
                var promedioFalso = (parseInt($(this).val()) / reactivos)* 10 ;
                falsa += promedioFalso < 5 ? 5 : promedioFalso > 10 ? 10 : promedioFalso;
               
            });

            $(this).parents('tr').find('[data-examen-total="' + id + '"]').html(sumatoria);
            total = total == 0 ? 1 : total;
            falsa = falsa / total;
                     
            var promedio = sumatoria * 10 / reactivos / total;
            
            $(this).html((falsa < 5 ? 5 : falsa > 10 ? 10 : falsa.toFixed(1)) + (falsa != promedio ? '(' + promedio.toFixed(1) + ')' : ''));

        });


        // Calificaciones de todos los temas de cada alumno en el examen
        $(selector).find('[data-examen-id="' + id + '"][data-total-agregado] [data-alumno-total]').each(function () {
            var numeroAspectos = $(selector).find('[data-examen-id="' + id + '"]').length - 1;
            var calificacion = 0;
      
            $(selector).find('[data-examen-id="' + id + '"] [data-alumno-id="' + this.getAttribute('data-alumno-total') + '"][data-alumno-tema] input').each(function () {
                calificacion += parseInt(this.value);
                
            });
            var rea =  parseInt($('[data-total-reactivos="' + id + '"]').text());
            var promedio = (calificacion  / parseInt($('[data-total-reactivos="' + id + '"]').text())*100);
             
            
            $(this).html(promedio < 50 || promedio > 100 ? (promedio > 100 ? 100 : 50) + ' (' + promedio.toFixed(1) + ')' : promedio.toFixed(1)).attr('data-real', promedio < 50 ? 50 : promedio > 100 ? 100 : promedio.toFixed(1));
        });

        // Aprobados y reprobados
        $(selector).find('[data-alumno-aprobado]').each(function () {
            var alumno = this.getAttribute('data-alumno-aprobado');
            var aprobados = 0, reprobados = 0;
            $(selector).find('[data-examen-id][data-total-agregado] [data-alumno-total="' + alumno + '"]').each(function () {

                aprobados += parseFloat($(this).attr('data-real')) >= 60 ? 1 : 0;
                reprobados += parseFloat($(this).attr('data-real')) < 60 ? 1 : 0;
            });

            $(selector).find('[data-alumno-aprobado="' + alumno + '"]').html(aprobados);
            $(selector).find('[data-alumno-reprobado="' + alumno + '"]').html(reprobados);
        });

    }

    var obtenerPosicion = function (selector, fecha) {
        var list = $(selector).find('[data-examen-fecha][data-visible="true"]').get();
        var date = fecha.toDate2();
        var tr = undefined;
        for (var m = 0; m < list.length - 1; m++) {

            if (date <= list[m].getAttribute('data-examen-fecha').toDate2() && date > list[m + 1].getAttribute('data-examen-fecha').toDate2()) {
                tr = list[m + 1];
                break;
            }
            else if (date > list[m].getAttribute('data-examen-fecha').toDate2() && m == 0) {
                tr = list[m];
                break;
            }
        }

        return tr;
    }

    var ajustarRows = function (selector) {
        var ids = [];

        $(selector).find('[data-examen-id]').each(function () {
            if (ids.indexOf(this.getAttribute('data-examen-id')) == -1) ids.push(this.getAttribute('data-examen-id'));
        });

        // eliminamos todos los rows de total agregados
        $(selector).find('[data-total-agregado]').remove();

        // Ajustar los rowspan
        var index = 1;
        ids.map(function (id) {

            // agregar row para totales a cada conjunto de examen
            $(selector).find('[data-examen-id="' + id + '"]:last').after('<tr data-total-agregado="" data-examen-id="' + id + '"><td colspan="3" rowspan=""></td><th><div class="w100">Calificación</div></th><td><div data-total-reactivos="'+id+'" class="w100">0</div></th>' + tdsTotales + '</tr>');

            var total = $(selector).find('[data-examen-id="' + id + '"]').length;

            $(selector).find('[data-examen-id="' + id + '"]').each(function (i, k) {
                if (i == 0) {
                    $(k).attr('data-visible', true);
                    $(k).find('td[rowspan]').removeClass('hide').attr('rowspan', total);
                    $(k).find('td:first').html('<div class="w50"><a><span data-option="download" class="fa fa-download visor-oculto" title="Descargar examen"></span></a><a><span data-option="editar" class="fa fa-edit  visor-oculto" title="Editar examen"></span></a><a><span data-option="eliminar" class="fa fa-trash  visor-oculto" title="Eliminar examen"></span></a> ' + index + '</div>');
                    index++;
                }
                else {
                    $(k).attr('data-visible', false);
                    $(k).find('td[rowspan]').addClass('hide');
                }
            });


            actualizarTotales(id);
        });

    }

    this.nuevoTemplate = _require("../app/bimestre/templates/cargarExamen.js"); 
    this.nuevo = function (selector, estado) {
        ConfirmDialog.show({
            title: 'Nuevo examen',
            text: _a.nuevoTemplate({ IDExamen: _uuid.v4() }),
            closeModalOnAction: false,
            callback: function (result) {
                if (result == true) $('#frmExamen').submit();
                else ConfirmDialog.hide();
            },
            beforeOpen: function () {
                var form = $('#frmExamen');
                $.validator.unobtrusive.parse(form);

                pluginDatepicker(form.find('[name="FechaEntrega"]'));

                form.find('#Bimestre').val(_bimestre);
                form.find('#Grupo').val(_grupo);
                form.find('#FechaEntrega').val(formatDate(new Date()));
                form.find('[data-option="pregunta"]').click();
                form.find('[name="Titulo"]').val(form.find('[name="Tipo"]').val() + ' ' + _bimestre);
                form.on("change", "[data-no-instrucciones]", function (ev) {
                    var $me = $(this)
                    var $related = form.find("[name='" + $me.data("no-instrucciones") + "']");
                    var isFirstQuestion = form.find("[data-pregunta]").eq(0).get(0) == $me.parents("[data-pregunta]").get(0);
                    if (!isFirstQuestion) {
                        $related.prop("disabled", $me.is(":checked"));
                    } else {
                        $me.prop("checked", false);
                        AlertError("No puede deshabilitar las instrucciones de la primera pregunta");
                    }
                })

                form.submit(function (e) {
                    e.preventDefault();

                    if ($(this).find('[data-pregunta]').length == 0) {
                        AlertWarning('Debes capturar por lo menos una pregunta', 'Exámenes');
                        return;
                    }

                    if ($(this).valid()) {
                        let values = form.serializeArray();
                        let item = { Temas: [] };
                        let examenTemaRegex = /ExamenTema\[(\d+)\]\.(\w+)/;
                        let isNumeric = val => /^\d+$/.test(val);

                        values.forEach(v => {
                            if (v.name == "FechaEntrega") {
                                item[v.name] = v.value.replace(/\//g, "-");
                                item["FechaEntregaDesplegable"] = item[v.name];
                            }
                            else if (v.name == "Grupo") {
                                item["IDGrupo"] = v.value;
                            }
                            else if (v.name.startsWith("ExamenTema[")) {
                                let token = v.name.replace(examenTemaRegex, "$1.$2").split(".");
                                let idx = +token[0];
                                let nme = token[1];

                                if (idx >= item.Temas.length) item.Temas.splice(idx, 0, {});
                                if (nme == "IDTema") {
                                    item.Temas[idx][nme] = _uuid.v4();
                                } 
                                else if (isNumeric(v.value)) {
                                    item.Temas[idx][nme] = +v.value;
                                }
                                else {
                                    item.Temas[idx][nme] = v.value;
                                }
                            }
                            else if (isNumeric(v.value)) {
                                item[v.name] = +v.value;
                            }
                            else {
                                item[v.name] = 
                                    v.value == "true" ? true :
                                    v.value == "false" ? false : v.value;
                            }
                        })
                        let alumnos = __smtdb__.alumnos
                            .filter(alumno => alumno.IDGrupo == _grupo && alumno.Deleted !== true)
                            .map(alumno => ({
                                IDAlumno: alumno.IDAlumno,
                                Calificacion: 0,
                                FechaSync: (new Date()).toISOString()
                            }));

                        item.FechaSync = (new Date()).toISOString();
                        item.Temas = item.Temas.map(t => {
                            t.Alumnos = JSON.parse(JSON.stringify(alumnos))
                            return t;
                        })

                        __smtdb__.examenes.push(item)
                        _Logued.set(__smtdb__).then(() => {
                            _a.generar(selector, item, true, true);
                            ConfirmDialog.hide();
                            Examen.desplegarResultados(_grupo, '#tabla-examen');
                        })
                    }

                }).validate({ ignore: '.hide' });

                form.find('[name="Tipo"]').change(function () {
                    form.find('[name="Titulo"]').val(form.find('[name="Tipo"]').val() + ' ' + _bimestre);
                });
            }

        });
    }

    this.editar = function (selector, data) {
        ConfirmDialog.show({
            title: 'Editar examen',
            text: _a.nuevoTemplate(data[0]),
            closeModalOnAction: false,
            callback: function (result) {
                if (result == true) $('#frmExamen').submit();
                else ConfirmDialog.hide();
            },
            beforeOpen: function () {
                var form = $('#frmExamen');

                $.validator.unobtrusive.parse(form);
                pluginDatepicker(form.find('[name="FechaEntrega"]'));

                form.find('#Grupo').val(_grupo); //IDGrupo
                form.find('#Bimestre').val(_bimestre); //Numero de bimestre

                for (var m in data[0]) {
                    form.find('[name="' + m + '"]').val(data[0][m]);
                }

                data.map(function (e) {
                    e.index = form.find('[data-pregunta]').length;
                    form.find('#preguntas').append(generarPregunta(e));
                });

                form.on("change", "[data-no-instrucciones]", function () {
                    var $me = $(this)
                    var $related = form.find("[name='" + $me.data("no-instrucciones") + "']");
                    var isFirstQuestion = form.find("[data-pregunta]").eq(0).get(0) == $me.parents("[data-pregunta]").get(0);
                    if (isFirstQuestion) {
                        $me.prop("checked", false);
                        AlertError("No puede deshabilitar las instrucciones de la primera pregunta");
                    } else {
                        $related.prop("disabled", $me.is(":checked"));
                    }
                });

                form.submit(function (e) {
                    e.preventDefault();

                    if ($(this).valid()) {
                        let values = form.serializeArray();
                        let item = __smtdb__.examenes.filter(e => e.IDExamen == data[0].IDExamen);
                        let examenTemaRegex = /ExamenTema\[(\d+)\]\.(\w+)/;
                        let temas = []

                        item = item[0];
                        values.forEach(v => {
                            if (v.name == "FechaEntrega") {
                                item[v.name] = v.value.replace(/\//g, "-");
                                item["FechaEntregaDesplegable"] = item[v.name];
                            }
                            else if (v.name == "Grupo") {
                                item["IDGrupo"] = v.value;
                            }
                            else if (v.name.startsWith("ExamenTema[")) {
                                let token = v.name.replace(examenTemaRegex, "$1.$2").split(".");
                                let idx = +token[0];
                                let nme = token[1];

                                if (idx >= temas.length) temas.splice(idx, 0, {});
                                if (nme == "IDTema") {
                                    temas[idx][nme] = v.value;
                                } 
                                else if (nme == "Reactivos") {
                                    temas[idx][nme] = +v.value;
                                }
                                else {
                                    temas[idx][nme] = v.value;
                                }
                            }
                            else if (v.name == "Bimestre") {
                                item[v.name] = +v.value;
                            }
                            else {
                                item[v.name] = 
                                    v.value == "true" ? true :
                                    v.value == "false" ? false : v.value;
                            }
                        })

                        // editar y quitar temas existentes
                        let temasIds = values.filter(i => i.name.indexOf("IDTema") >= 0).map(i => i.value);
                        item.FechaSync = (new Date()).toISOString();
                        item.Temas = item.Temas
                            .filter(t => temasIds.indexOf(t.IDTema) >= 0)
                            .map(t => {
                                let tema = temas[temas.findIndex(i => i.IDTema == t.IDTema)]; 
                                return $.extend(true, {
                                    FechaSync: (new Date()).toISOString()
                                }, t, tema);
                            });

                        // agregar nuevos temas
                        let toAdd = temasIds.filter(id => !item.Temas.filter(t => t.IDTema == id).length);
                        if (toAdd.length) {
                            let alumnos = __smtdb__.alumnos
                                .filter(alumno => alumno.IDGrupo == _grupo && alumno.Deleted !== true)
                                .map(alumno => ({
                                    IDAlumno: alumno.IDAlumno,
                                    Calificacion: 0,
                                    FechaSync: (new Date()).toISOString()
                                }));

                            toAdd.forEach(id => {
                                let tema = temas[temas.findIndex(i => i.IDTema == id)];

                                item.Temas.push($.extend(true, tema, {
                                    IDTema: _uuid.v4(),
                                    Alumnos: alumnos.slice(),
                                    FechaSync: (new Date()).toISOString()
                                }))
                            });
                        }
                        
                        // guardar todo
                        _Logued.set(__smtdb__).then(db => {
                            $('[data-examen-id="' + item.IDExamen + '"]').remove();
                            _a.generar(selector, item, true, true);
                            ConfirmDialog.hide();

                            Examen.desplegarResultados(_grupo, '#tabla-examen');
                        })

                        // TODO: para envio de archivos
                        // generarFormData(form)
                    }
                });
            }

        });
    }

    this.eliminar = function (selector, id) {
        let remove = () => {
            let index = __smtdb__.examenes.findIndex(e => e.IDExamen == id);
            __smtdb__.examenes[index].Deleted = true;
            __smtdb__.examenes[index].FechaSync = (new Date()).toISOString();
            _Logued.set(__smtdb__).then(() => {
                $('[data-examen-id="' + id + '"]').removeConEfecto();
                AlertSuccess('Se ha eliminado el examen', 'Examenes');
                ConfirmDialog.hide();
                Examen.desplegarResultados(_grupo, selector);
            });
        };

        ConfirmDialog.show({
            title: 'Eliminar Examen',
            text: '<h3 class="text-center">Esta intentando eliminar este examen permanentemente, el cual ya no se podrá recuperar. ¿Desea continuar?</h3>',
            positiveButtonClass: 'btn btn-danger',
            positiveButtonText: 'Si',
            negativeButtonClass: 'btn btn-success',
            negativeButtonText: 'No',
            closeModalOnAction: false,
            callback: result => {
                if (result == true) remove();
                else ConfirmDialog.hide();
            }
        });
    }

    var generarPregunta = function (data) {
        var template = $('[data-template="pregunta"]').html();

        if (data != undefined) {
            template = template.format(data);
        }

        var pregunta = $(template);

        for (var m in data) {
            pregunta.find('[name$="' + m + '"]').val(data[m]);
        }

        pregunta.find('select[name$="TipoTema"]').change(function () {
                    

                if($(this).val() == 'Multiple'){
                    $('#frmExamen').find('label#pregunta').show();  
                    $('#frmExamen').find('textarea#tApregunta').show();
                }else{
                    $('#frmExamen').find('label#pregunta').hide();  
                    $('#frmExamen').find('textarea#tApregunta').hide(); 
                }
                if($(this).val() == 'Laguna'){
                    $('#frmExamen').find('label#pregunta').show();  
                    $('#frmExamen').find('textarea#tApregunta').show();
                }else{
                    $('#frmExamen').find('label#pregunta').hide();  
                    $('#frmExamen').find('textarea#tApregunta').hide(); 
                }
                if($(this).val() == 'Abierta'){
                    $('#frmExamen').find('label#pregunta').show();  
                    $('#frmExamen').find('textarea#tApregunta').show();
                }else{
                    $('#frmExamen').find('label#pregunta').hide();  
                    $('#frmExamen').find('textarea#tApregunta').hide(); 
                }
                if($(this).val() == 'Columnas'){
               
                    $('#frmExamen').find('label#pregunta').hide();  
                    $('#frmExamen').find('textarea#tApregunta').hide(); 
                }else{
                    $('#frmExamen').find('label#pregunta').show();  
                    $('#frmExamen').find('textarea#tApregunta').show();
                }
         

            $(this).parents('[data-pregunta]').find('[data-respuesta]').addClass('hide');
            var activo = $(this).parents('[data-pregunta]').find('[data-respuesta*="' + $(this).val() + '"]');
                activo.removeClass('hide');

            if ($(this).val() == 'Multiple' && activo.find('textarea').val() == '') {
                activo.find('textarea').val('a)');
            }
            if ($(this).val() == 'Columnas' && activo.find('textarea').val() == '') {
           
              $('#frmExamen').find('textarea#col1').val("a)");
              $('#frmExamen').find('textarea#col2').val("1)");
        
            }
            
        }).change();

        if (data.UrlArchivo != null) {
            pregunta.find('input:file').after('<img src="' + data.UrlArchivo + '" class="img-thumbnail" >');
        }
   
        return pregunta;
    }



    var generarFormData = function (form) {
        var formData = new FormData();

        form.find('[name]').each(function () {
            if ($(this).is(':file'))
                formData.append(this.name, this.files[0]);
            else {
                formData.append(this.name, this.disabled ? "" : this.value);
            }
        });
        return formData;
    }

    var ajustarIndex = function () {
        $('#frmExamen').find('[data-pregunta]').each(function (i,k) {
            $(this).find('[name]').each(function () {
                this.name = 'ExamenTema['+ i +']' + this.name.substring(this.name.indexOf('.'),this.name.length);

            });
        });
    }
   
    // Cambiar el estado inmediatamente al seleccionar opcion
    $('body:not(.visualizando)').delegate('[data-alumno-id][data-alumno-tema] input', 'change', function () {

        if ($(this).val() >= 0 && $(this).val() <= 100) {
            var alumno = $(this).parents('td').attr('data-alumno-id');
            var tema = $(this).parents('td').attr('data-alumno-tema');

            actualizarCalificacion(alumno, $(this).val(), tema, $(this).parents('[data-examen-id]').attr('data-examen-id'));
            $(this).removeClass('input-validation-error');
        }
        else
            $(this).addClass('input-validation-error');
    });

    $('body:not(.visualizando)').delegate('[data-examen-id] [data-option="eliminar"]', 'click', function () {
        var tr = $(this).parents('[data-examen-id]');
        _a.eliminar(tr.parents('table'), tr.attr('data-examen-id'));
    });
    $('body:not(.visualizando)').delegate('[data-examen-id] [data-option="editar"]', 'click', function () {
        var tr = $(this).parents('[data-examen-id]');
        var data = [];

        $('[data-examen-id="' + tr.attr('data-examen-id') + '"]:not([data-total-agregado])').each(function () {
            data.push($(this).getDataAsObject());
        });

        _a.editar(tr.parents('table'), data);
    });

    // Agregar preguntas
    $('body:not(.visualizando)').delegate('[data-option="pregunta"]', 'click', function () {
        
        var total = $('form [data-pregunta]').length;
        var total2 = $('form [name$="Instrucciones"]:not([disabled])').length;

        $('#preguntas').append(generarPregunta({
            index: total,
            Nombre: 'Tema / Subtema ' + (total + 1)
        }));

        $('#preguntas [data-pregunta] .panel-collapse ').removeClass('in');
        $('#preguntas [data-pregunta]:last .panel-collapse ').addClass('in');
        $('#preguntas [data-pregunta]:last').find('[name$="Instrucciones"]').val(romanize(total2 + 1) + '. ');

        $.validator.unobtrusive.parse($('#frmExamen'));
    });

    $('body:not(.visualizando)').delegate('[name$="Nombre"]', 'keyup', function () {

        $(this).parents('[data-pregunta=""]').find('span.nombre').html(this.value);
    });

    $('body:not(.visualizando)').delegate('#frmExamen [data-option="eliminar-pregunta"]', 'click', function () {
        var pregunta =  $(this).parents('[data-pregunta]');

        $.confirm({
            title: 'Eliminar pregunta',
            content: '¿Esta seguro de eliminar esta pregunta?',
            confirmButton: 'Si',
            confirmButtonClass: 'btn-danger',
            cancelButton: 'No',
            icon: 'fa fa-question-circle',
            animation: 'scale',
            confirm: function () {
                pregunta.remove();
                ajustarIndex();
            }
        });
        
    });

    $('body:not(.visualizando)').delegate('[data-option="download"]', 'click', function () {
        r
         myWindow = open('http://app.mastertools.mx/examenes/download?examen=' + $(this).parents('[data-examen-id]').attr('data-examen-id'));

    });
    
    // Se precarga template 
    rowExamen = _require('../app/bimestre/templates/rowExamen.js');
    this.desplegarResultados(_grupo, '#tabla-examen');
    this.Imprimir = function () {
        let url = __server__ + '/examenes/imprimir?grupo=' + _grupo + '&bimestre=' + _bimestre;
        let download = _Downloader.download(url);
        Loading("...")
        download.then(res => {
            Loading();
            open(res.file); 
        }).catch(err => {
            console.error(err)

            _Downloader.cached(url).then(res => {
                Loading();
                open(res.file);
                AlertError("Mostrando versión desactualizada, conectese a internet para ver una versión actualizada")
            }).catch(err => {
                AlertError("Conectese a internet para descargar la version para imprimir")
                Loading();
                console.error(err)
            })
        })
    }
}

Examen = new Examen();

$('body:not(.visualizando)').delegate('[data-examen="nuevo"]', 'click', function () {
    Examen.nuevo('#tabla-examen', 1);
});


$('body:not(.visualizando)').delegate('#frmExamen textarea', 'keypress', function (e) {
    if (e.keyCode == 13) {
        var texts = $(this).val().split('\n');
        var reg = /^(\w|\d)(\)|\.-?)/ig;
       
        var ultimaLinea = texts[texts.length - 1];
        var sinEspacios = ultimaLinea.trim();
        
      
        if (reg.test(sinEspacios)) {

            var matches = reg.exec(sinEspacios);
            
           
            if (matches == null) matches = reg.exec(sinEspacios); // Nose porque la primera vez da null y la segunda ya da bien el resultado...
                
            
                if(!isNaN(matches[1])){
                
                        
                    acumulador=acumulador+1;
                    var resultado = acumulador+")";
                  
                   
               
                
               
            }else{
            var valor =matches[1].charCodeAt(0);
                        
           //matches[1] siempre es el primer numero y matches[2] siempre es el )
            var tipo = matches[2];
            var resultado =  String.fromCharCode(valor + 1) + tipo;
            }
            
            
            // Ahora detectar espacios
            var espacios = /\s+?_+/;
            if (espacios.test(ultimaLinea)) {
                var match = espacios.exec(ultimaLinea);
               
                if (match == null) match =espacios.exec(ultimaLinea);
                resultado += match[0];
               
            }
            
              
            $(this).val($(this).val() + '\n' + resultado + ' ');
            return false;

        }

    }acumulador=1;
});

var cargarComponenteUpload = function (select) {
    $(select).addClass('dropzone').dropzone({
        url: '/Home',
        maxFiles: 1,
        acceptedFiles: 'image/jpg, image/jpeg, image/png',
        dictDefaultMessage: 'Arrastre o seleccione la imagen a subir',
        init: function () {
            this.on('success', function (file) {
                var reader = new FileReader();
                reader.onload = function (e) {

                    $(select).before('<img data-preview="' + $(select).attr('data-imagen') + '" src="{0}" class="option-container default" />'.format([e.target.result]));

                    $(select).parent().find('[data-preview]').cropper({
                        aspectRatio: 'free',
                        movable: true,
                        zoomable: true,
                        rotatable: false,
                        scalable: false,
                        strict: false,
                        guides: false,
                        highlight: false,
                        dragCrop: false,
                        cropBoxMovable: true,
                        cropBoxResizable: true,
                        minCropBoxWidth: 200,
                        minCropBoxHeight: 112.50
                    });

                    $(select).hide();

                }
                reader.readAsDataURL(file);
            });
            this.on("error", function (file) {
                if (!file.accepted) {
                    this.removeFile(file);
                    AlertError('Solo se permiten imagenes');
                }
            });
        }
    });
}