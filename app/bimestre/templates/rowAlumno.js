module.exports = model => `

<tr data-alumno-id="${model.IDAlumno}" data-alumno-nombre="${model.ApellidoPaterno} ${model.ApellidoMaterno} ${model.Nombre}" class="${model.Color}" style="background-color:${model.ColorSemaforo}">
    <td style="background-color:${model.ColorSemaforo}">${model.Semaforo}</td>
    <td>${model.ApellidoPaterno}</td>
    <td>${model.ApellidoMaterno}</td>
    <td>${model.Nombre}</td>
    <td class="text-center">
        <div class="btn-group">
            <button data-alumno="detalles" onclick="javascript:NavigateTo('alumno.html', { IDAlumno: '${model.IDAlumno}' })" type="button" class="btn btn-default" style="width:100px">Ver</button>
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu visor-oculto">
                <li><a href="javascript:void(0)" onclick="Alumnos.asignarTutor('${model.IDAlumno}')" data-alumno="ver-como-tutor" >Asignar Padre de Familia</a></li>
                <li><a href="#" data-alumno="editar" onclick="Alumnos.cargarAlumno('${model.IDAlumno}')">Editar</a></li>
                <li><a href="#" data-alumno="eliminar" onclick="Alumnos.eliminarAlumno('${model.IDAlumno}')">Eliminar</a></li>
            </ul>
        </div>
    </td>
</tr>

`