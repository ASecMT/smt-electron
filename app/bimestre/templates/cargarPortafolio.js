module.exports = (model, tipoPortafolios) => {
  let tipos = tipoPortafolios.map(t => `<option ${(model.IDTipoPortafolio == t.IDTipoPortafolio ? "selected":"")} value="${t.IDTipoPortafolio}">${t.Nombre}</option>`);

  return `
    <form action="/" id="frmPortafolio" method="post">
    <input id="Bimestre" name="Bimestre" type="hidden" value="${model.Bimestre}" />
    <input id="IDGrupo" name="IDGrupo" type="hidden" value="${model.IDGrupo}" />
    <input data-val="true" data-val-required="El campo IDPortafolio es obligatorio." id="IDPortafolio" name="IDPortafolio" type="hidden" value="${model.IDPortafolio}" />
    <div class="col-lg-12 form-group">
      <label for="Nombre">Nombre</label>
      <input class="form-control" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-required="Es obligatorio" id="Nombre" name="Nombre" type="text" value="${model.Nombre}" />
      <span class="field-validation-valid" data-valmsg-for="Nombre" data-valmsg-replace="true"></span>
    </div>
    <div class="col-lg-6 form-group">
      <label for="FechaEntrega">Fecha Entrega</label>
      <input type="text" id="FechaEntrega" name="FechaEntrega" class="form-control" required value="${model.Fecha}" />
      <span class="field-validation-valid" data-valmsg-for="FechaEntrega" data-valmsg-replace="true"></span>
    </div>
    <div class="col-lg-6 form-group">
      <label for="IDTipoPortafolio">Instrumentos</label>
      <select class="form-control" data-val="true" data-val-required="Es obligatorio" id="IDTipoPortafolio" name="IDTipoPortafolio">
      ${tipos}
      </select>
      <span class="field-validation-valid" data-valmsg-for="IDTipoPortafolio" data-valmsg-replace="true"></span>
    </div>
    <div class="col-lg-12 form-group">
      <label for="Descripcion">Descripci&#243;n</label>
      <textarea class="form-control" cols="20" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-required="Es obligatorio" id="Descripcion" name="Descripcion" rows="2">
        ${(model.Descripcion || "")}
      </textarea>
      <span class="field-validation-valid" data-valmsg-for="Descripcion" data-valmsg-replace="true"></span>
    </div>
        <table class="table table-condensed table-striped table-hover text-center">
            <thead>
                <tr>
                    <th colspan="4">Aspectos de evaluación</th>
                </tr>
                <tr>
                    <th class="col-lg-4">Aspecto</th>
                    <th class="col-lg-6">Criterio de evaluación</th>
                    <th class="col-lg-2">Activo</th>
                </tr>
            </thead>
            <tbody>
                <tr class="resaltar">
                    <td>
                        <input class="form-control form-control-oculto" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" id="Aspecto1" name="Aspecto1" placeholder="Aspecto1" required="required" type="text" value="${model.Aspecto1}" />
                        <span class="field-validation-valid" data-valmsg-for="Aspecto1" data-valmsg-replace="true"></span>
                    </td>
                    <td>
                        <textarea class="form-control form-control-oculto" cols="20" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 8000" data-val-length-max="8000" id="Criterio1" name="Criterio1" placeholder="Ejemplo:
    1 - Si lo incluye
    2 - Faltante
    3 - No lo incluye" required="required" rows="2">${model.Criterio1}
    </textarea>
                        <span class="field-validation-valid" data-valmsg-for="Criterio1" data-valmsg-replace="true"></span>
                    </td>
                    <td><input checked="checked" data-aspecto="1" data-val="true" data-val-required="El campo Activo1 es obligatorio." id="Activo1" name="Activo1" type="checkbox" ${(model.Activo1 ? "checked" : "")} /></td>
                </tr>
                <tr class="resaltar">
                    <td>
                        <input class="form-control form-control-oculto" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" id="Aspecto2" name="Aspecto2" placeholder="Aspecto 2" required="required" type="text" value="${model.Aspecto2}" />
                        <span class="field-validation-valid" data-valmsg-for="Aspecto2" data-valmsg-replace="true"></span>
                    </td>
                    <td>
                        <textarea class="form-control form-control-oculto" cols="20" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 8000" data-val-length-max="8000" id="Criterio2" name="Criterio2" placeholder="Ejemplo:
    1 - Si lo incluye
    2 - Faltante
    3 - No lo incluye" required="required" rows="2">${model.Criterio2}
    </textarea>
                        <span class="field-validation-valid" data-valmsg-for="Criterio2" data-valmsg-replace="true"></span>

                    </td>
                    <td><input checked="checked" data-val="true" data-val-required="El campo Activo2 es obligatorio." id="Activo2" name="Activo2" type="checkbox" ${(model.Activo2 ? "checked" : "")} /></td>
                </tr>
                <tr class="resaltar">
                    <td>
                        <input class="form-control form-control-oculto ignore" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" id="Aspecto3" name="Aspecto3" placeholder="Aspecto 3" required="required" type="text" value="${model.Aspecto3}" />
                        <span class="field-validation-valid" data-valmsg-for="Aspecto3" data-valmsg-replace="true"></span>
                    </td>
                    <td>
                        <textarea class="form-control form-control-oculto ignore" cols="20" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 8000" data-val-length-max="8000" id="Criterio3" name="Criterio3" placeholder="Ejemplo:
    1 - Si lo incluye
    2 - Faltante
    3 - No lo incluye" required="required" rows="2">${model.Criterio3}
    </textarea>
                        <span class="field-validation-valid" data-valmsg-for="Criterio3" data-valmsg-replace="true"></span>
                    </td>
                    <td><input data-val="true" data-val-required="El campo Activo3 es obligatorio." id="Activo3" name="Activo3" type="checkbox" ${(model.Activo3 ? "checked" : "")} /></td>
                </tr>
                <tr  class="resaltar">
                    <td>
                        <input class="form-control form-control-oculto ignore" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" id="Aspecto4" name="Aspecto4" placeholder="Aspecto 4" required="required" type="text" value="${model.Aspecto4}" />
                        <span class="field-validation-valid" data-valmsg-for="Aspecto4" data-valmsg-replace="true"></span>
                    </td>
                    <td>
                        <textarea class="form-control form-control-oculto ignore" cols="20" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 8000" data-val-length-max="8000" id="Criterio4" name="Criterio4" placeholder="Ejemplo:
    1 - Si lo incluye
    2 - Faltante
    3 - No lo incluye" required="required" rows="2">${model.Criterio4}
    </textarea>
                        <span class="field-validation-valid" data-valmsg-for="Criterio4" data-valmsg-replace="true"></span>
                    </td>
                    <td><input data-val="true" data-val-required="El campo Activo4 es obligatorio." id="Activo4" name="Activo4" type="checkbox" ${(model.Activo4 ? "checked" : "")} /></td>
                </tr>
                <tr class="resaltar">
                    <td>
                        <input class="form-control form-control-oculto ignore" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" id="Aspecto5" name="Aspecto5" placeholder="Aspecto 5" required="required" type="text" value="${model.Aspecto5}" />
                        <span class="field-validation-valid" data-valmsg-for="Aspecto5" data-valmsg-replace="true"></span>
                    </td>
                    <td>
                        <textarea class="form-control form-control-oculto ignore" cols="20" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 8000" data-val-length-max="8000" id="Criterio5" name="Criterio5" placeholder="Ejemplo:
    1 - Si lo incluye
    2 - Faltante
    3 - No lo incluye" required="required" rows="2">${model.Criterio5}
    </textarea>
                        <span class="field-validation-valid" data-valmsg-for="Criterio5" data-valmsg-replace="true"></span>
                    </td>
                    <td><input data-val="true" data-val-required="El campo Activo5 es obligatorio." id="Activo5" name="Activo5" type="checkbox" ${(model.Activo5 ? "checked" : "")} /></td>
                </tr>
            </tbody>
        </table>
    </form>`;
}