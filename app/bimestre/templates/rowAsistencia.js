module.exports = model => `
<tr data-grupo="${model.grupo}" data-sesion-fecha="${model.fecha}" data-sesion-id="${model.id}" class="resaltar">
    <td><div class="w50"><a><span data-asitencia-option="eliminar" class="fa fa-trash visor-oculto" title="Eliminar sesión"></span></a>${model.num}</div></td>
    <td>
        <input name="fecha" type="text" class="form-control form-control-oculto" placeholder="Fecha de Sesion" value="${model.fecha}" style="width:150px" />
    </td>
    ${model.tds}
    <td ><div data-sesion-total="${model.id}" class="w100">0</div></td>
    <td><div  data-sesion-promedio="${model.id}" class="w100">0</div></td>
    <td>
        <textarea name="observacion" class="form-control form-control-oculto"  placeholder="Observaciones" style="width:200px">${model.observacion || ""}</textarea>
    </td>
</tr>
`