module.exports = model => `

<tr data-tema="${model.IDTema}" data-examen-fecha="${model.FechaEntregaDesplegable}" data-examen-id="${model.IDExamen}" data-orden="1" class="resaltar">
  <td rowspan="1">${model.num}</td>
  <td rowspan="1"><div class="w100">${model.FechaEntregaDesplegable}</div></td>
  <td rowspan="1"><div class="w100">${(model.Titulo || "N/A")}</div></td>
  <td rowspan="1"><div class="w100">${model.Tipo}</div></td>
  <td rowspan="2"><div class="w100">${model.Nombre} <span data-toggle="tooltip" data-placement="top" title="${model.Pregunta}"><a class="fa fa-question-circle-o"></a></span></div></td>
  <td><div class="w100">${model.Instrucciones}</div></td>
  <td><div class="w100" data-reactivos="">${model.Reactivos}</div></td>
  ${model.tds}
  <td><div data-examen-total="${model.IDExamen}" class="w100">0</div></td>
  <td><div data-examen-promedio="${model.IDExamen}" class="w100">0</div></td>
</tr>

`