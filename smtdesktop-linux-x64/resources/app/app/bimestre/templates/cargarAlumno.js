module.exports = model => `
<script src="../scripts/jquery.validate.min.js"></script>
<script src="../scripts/jquery.validate.unobtrusive.min.js"></script>

<form method="post">
    <input data-val="true" data-val-number="The field IDAlumno must be a number." data-val-required="El campo IDAlumno es obligatorio." id="IDAlumno" name="IDAlumno" type="hidden" value="${model.IDAlumno || ""}" />
    <input data-val="true" data-val-number="The field IDGrupo must be a number." data-val-required="El campo IDGrupo es obligatorio." id="IDGrupo" name="IDGrupo" type="hidden" value="${model.IDGrupo || ""}" />
    <div class="col-md-12 col-sm-12">
        <div class="col-md-6 col-xs-6 form-group">
            <label for="Nombre">Nombre</label><br />
            <input class="form-control col-12" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-required="Es obligatorio" id="Nombre" name="Nombre" type="text" value="${model.Nombre || ""}" /><br />
            <span class="field-validation-valid" data-valmsg-for="Nombre" data-valmsg-replace="true"></span>
        </div>
        <div class="col-md-6 col-xs-6 form-group">
            <label for="ApellidoPaterno">Apellido Paterno</label><br />
            <input class="form-control col-12" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-required="Es obligatorio" id="ApellidoPaterno" name="ApellidoPaterno" type="text" value="${model.ApellidoPaterno || ""}" /><br />
            <span class="field-validation-valid" data-valmsg-for="ApellidoPaterno" data-valmsg-replace="true"></span>
        </div>
        <div class="col-md-6 col-xs-6 form-group">
            <label for="ApellidoMaterno">Apellido Materno</label><br />
            <input class="form-control col-12" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-required="Es obligatorio" id="ApellidoMaterno" name="ApellidoMaterno" type="text" value="${model.ApellidoMaterno || ""}" /><br />
            <span class="field-validation-valid" data-valmsg-for="ApellidoMaterno" data-valmsg-replace="true"></span>
        </div>
        <div class="col-md-6 col-xs-6 form-group">
            <label for="Curp">CURP</label><br />
            <input class="form-control col-12" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-required="Es obligatorio" id="Curp" name="Curp" type="text" value="${model.Curp || ""}" /><br />
            <span class="field-validation-valid" data-valmsg-for="Curp" data-valmsg-replace="true"></span>
        </div>
        ${(model.EsTaller ? `
            <div class="col-md-6 col-xs-6 form-group">
                <label>Grupo</label>
                <input class="form-control col-12" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-regex="Formato Incorrecto (Ej. ABC, A, B)" data-val-regex-pattern="^[A-Za-z]{1,3}" data-val-required="Es obligatorio" id="Grupo" name="Grupo" required="true" type="text" value="${model.Grupo || ""}">                
                <span class="field-validation-valid" data-valmsg-for="Grupo" data-valmsg-replace="true"></span>
            </div>
        `:'')}
        <div class="col-md-6 col-xs-6 form-group">
            <label for="EsUSAER">Es NEE</label><br />
            <select class="form-control col-12" data-val="true" data-val-required="Es obligatorio" id="EsUSAER" name="EsUSAER">
                <option value="true">SI</option>
                <option selected="selected" value="false">NO</option>
            </select>
            <span class="field-validation-valid" data-valmsg-for="EsUSAER" data-valmsg-replace="true"></span>
        </div>
    </div>
</form>
`