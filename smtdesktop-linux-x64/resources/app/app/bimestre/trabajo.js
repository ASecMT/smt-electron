var Trabajo = new function () {
    var _a = this;
    var tdsCaptura = '',
        rowNumeros = '',
        rowNombres = '',
        rowInacisistencia = '',
        rowMedios='',
        rowAsistencias = '';
var trabajosA = [];

    var generarNombreCache = function () {
        return 'trabajo-' + _grupo + '-' + _bimestre;
    }

    this.listar = function () {
        let trabajos = __smtdb__.trabajos.filter(t => t.IDGrupo == _grupo && t.Bimestre == _bimestre && t.Deleted !== true);
        return Promise.resolve(trabajos);
        trabajosA.pus(trabajos);
        console.log(trabajosA.length);
    }

    this.generarTrabajoTemplate = _require('../app/bimestre/templates/rowTrabajo.js'); 
    this.generarTrabajo = function (selector, trabajo, grupo, focus, autoOrdenar) {
        let template = _a.generarTrabajoTemplate;
        trabajo.num = $(selector).find('[data-trabajo-id]').length + 1;
        trabajo.tds = tdsCaptura;
        trabajo.grupo = grupo;
        trabajosA.push(trabajo);
   

        var elementSesion = $(template(trabajo)).appendTo($(selector).find('tbody'));

        // Actualizar los tds a los datos reales
        trabajo.entrega.map(function (a) {
           
            elementSesion.find('[data-alumno-id="' + a.id + '"]').attr('data-alumno-trabajo-estado', a.estado).html('<div class="w100">'+obtenerHtmlEstado(a.estado)+'</div>');
        });

        // Se actualizan los totales de la sesion y la columna del alumno
        actualizarTotales(trabajo.id);

        if($('body').hasClass('visualizando') == false)
            pluginDatepicker(elementSesion.find('[name="fecha"]'), function (obj) {
                var tr = $(obj).parents('[data-trabajo-id]');

                _a.editar(tr.attr('data-trabajo-id'), tr.find('[name="fecha"]').val(), tr.find('[name="observacion"]').val(), tr.find('[name="nombre"]').val(), tr.find('[name="tipo"]').val(), tr.find('[name="actividad"]').val());

            });
        else
            elementSesion.find('input,textarea').attr('disabled',true)

        

        if (focus == true) {
            $('html, body').animate({
                scrollTop: elementSesion.offset().top
            }, 1000);

            elementSesion.resaltar('info', 3000)
                            .find('input:first')
                            .focus();
        }
  

    }
  
  
    this.desplegarResultadosTemplate = _require('../app/bimestre/templates/tablaTrabajo.js');
    this.desplegarResultados = function (grupo, selector) {

        // Se generan rows y tds en base a los alumnos

        // Estos html se generaran cada vez que se ejecute desplegar resultados, 
        // una vez generado ya todas las acciones que requieran estos html solo utilizaran el html que se genero
        rowNombres = '';
        rowNumeros = '';
        tdsCaptura = '';
        rowAsistencias = '';
        rowMedios = '';

        rowInacisistencia = '';

        Alumnos.listar(_grupo).then(function () {
            for (var i = 0; i < Alumnos.data.length; i++) {
                var alumn = Alumnos.data[i];
                rowNumeros += `<th>${i + 1}</th>`;
                rowNombres += `<th><div class="w100"><span data-alumno="${alumn.IDAlumno}" class="semaforo" style="background-color:${alumn.semaforo}"></span><span class="nombre">${alumn.ApellidoPaterno} ${alumn.ApellidoMaterno} ${alumn.Nombre}</span></div></th>`;
                rowInacisistencia += `<td><div data-alumno-noentrego="${alumn.IDAlumno}" class="w100">0</div></th>`;
                rowAsistencias += `<td><div data-alumno-entrego="${alumn.IDAlumno}" class="w100">0</div></th>`;
                rowMedios += `<td><div data-alumno-medio="${alumn.IDAlumno}" class="w100">0</div></th>`;
                tdsCaptura += `<td><div data-alumno-id="${alumn.IDAlumno}" data-alumno-trabajo-estado="" tabindex="0" class="w100"><span class="fa fa-check"></span></div></td>`;
            }

            let template = _a.desplegarResultadosTemplate;
            var datos = {
                numerosHeaders: rowNumeros,
                nombresHeaders: rowNombres,
                rowAsistencia: rowAsistencias,
                rowMedio: rowMedios,
                rowInacisistencia: rowInacisistencia
            };

            $(selector).html(template(datos));

            _a.listar(grupo).then(function (data) {
                data.map(function (s) {
                    _a.generarTrabajo(selector, s, grupo);
                });

                $(selector).find('.loading').remove();

                Alumnos.cargarSemaforo('trabajos');
            });
        });
    }

    var obtenerHtmlEstado = function (estado) {
        var estadosResultado = ['<span class="fa fa-close"></span>', '<span class="fa fa-check"></span>', '&frac12'];
        if (estado == null) {
            return '<span class="fa fa-close"></span>';
        }
        return estadosResultado[estado];
    }

    var actualizarAsistencia = function (alumno, sesion, estado, grupo) {
        if (estado == '' || alumno == '' || sesion == '') return;

        // Brincarse inmediatamente al otro elemento sin esperar a que termine
        $('[data-trabajo-id="' + sesion + '"] [data-alumno-id="' + alumno + '"]').focusout()
        if ($('[data-trabajo-id="' + sesion + '"] [data-alumno-id="' + alumno + '"]').parents('td:first').next().find('[tabindex]').length == 0) {
            // En el ultimo td ya no hace el brinco hacia el textarea
            $('[data-trabajo-id="' + sesion + '"] [data-alumno-id="' + alumno + '"]').parent().find('textarea').focus();
        }
        else {
            $('[data-trabajo-id="' + sesion + '"] [data-alumno-id="' + alumno + '"]').parents('td:first').next().find('[tabindex]').focus();
        }

        let trabajo = __smtdb__.trabajos.filter(t => t.id == sesion && t.IDGrupo == grupo);
     
        if (trabajo.length) {
            trabajo = trabajo[0];
            let entrega = trabajo.entrega.filter(s => s.id == alumno);
            if (entrega.length) {
                entrega = entrega[0];
                entrega.estado = +estado;
                entrega.FechaSync = (new Date()).toISOString();
            } else {
                trabajo.entrega.push({
                    id: alumno, 
                    estado: +estado,
                    FechaSync: (new Date()).toISOString()
                })
            }

            _Logued.set(__smtdb__).then(() => {
                // Se despliega la sesion actualizada con efecto
                $('[data-trabajo-id="' + sesion + '"] [data-alumno-id="' + alumno + '"]')
                    .attr('data-alumno-trabajo-estado', estado)
                    .html('<div class="w100">' +obtenerHtmlEstado(estado) + '</div>')
                    .resaltar('info', 800);

                actualizarTotales(sesion);
                Alumnos.cargarSemaforo('trabajos', alumno);                    
            })
        }
    }

    var actualizaDataEnCache = function () {
        // Se complica actualizar los datos, asi que se elimina el cache y la proxima vez se volvera a descargar todo
        Cache.vaciar(generarNombreCache());
    }

    var actualizarTotales = function (sesion) {
        var selector = $('[data-tabla="trabajo"]');

        Alumnos.data.map(function (alumno) {
            var totalAsistenciaAlumno = $(selector).find('[data-alumno-id="' + alumno.IDAlumno + '"][data-alumno-trabajo-estado="1"]').length;
            var totalFaltasAlumno = $(selector).find('[data-alumno-id="' + alumno.IDAlumno + '"][data-alumno-trabajo-estado="0"]').length;
            var totalMedio = $(selector).find('[data-alumno-id="' + alumno.IDAlumno + '"][data-alumno-trabajo-estado="2"]').length;

            $(selector).find('[data-alumno-entrego="' + alumno.IDAlumno + '"]').html(totalAsistenciaAlumno);
            $(selector).find('[data-alumno-noentrego="' + alumno.IDAlumno + '"]').html(totalFaltasAlumno);
            $(selector).find('[data-alumno-medio="' + alumno.IDAlumno + '"]').html(totalMedio);
        });

        var totalSesionAsistencia = $(selector).find('[data-trabajo-id="' + sesion + '"] [data-alumno-id][data-alumno-trabajo-estado="1"]').length*10;
        var totalnoentregado = $(selector).find('[data-trabajo-id="' + sesion + '"] [data-alumno-id][data-alumno-trabajo-estado="0"]').length*0;
        var totalmedio = $(selector).find('[data-trabajo-id="' + sesion + '"] [data-alumno-id][data-alumno-trabajo-estado="2"]').length*5;
        var totalSesionAlumnos = $(selector).find('[data-trabajo-id="' + sesion + '"] [data-alumno-id]').length;
        $(selector).find('[data-trabajo-total="' + sesion + '"]').html((totalSesionAsistencia + totalnoentregado + totalmedio)/10.0);
        $(selector).find('[data-trabajo-promedio="' + sesion + '"]').html((((totalSesionAsistencia + totalnoentregado + totalmedio) / (totalSesionAlumnos*10))* 100).toFixed(2) + '%');
    }

    var ordenar = function (selector) {

        var sort_by_date = function (a, b) {
            var dateA = a.getAttribute('data-trabajo-ticks');
            var dateB = b.getAttribute('data-trabajo-ticks');

            return dateA < dateB;
        }

        var list = $(selector).find('[data-trabajo-fecha]').get();
        list.sort(sort_by_date);
        for (var i = 0; i < list.length; i++) {
            list[i].parentNode.appendChild(list[i]);
            var hay = $(list[i]).find('td:first').find('span');
            if (hay.length == 0) {
                $(list[i]).find('td:first').find('div').append('<a><span data-trabajo-option="editardescripcion" class="fa fa-edit visor-oculto" title="Editar Descripción"></span></a><a><span data-asitencia-option="eliminar" class="fa fa-trash visor-oculto" title="Eliminar sesión"></span></a> ');
                
            }

        }
    }

    this.nuevo = function (selector, estado, tipo, actividad) {
        let trabajos = __smtdb__.trabajos;
        let fecha = new Date();

        let fechaSync = (new Date()).toISOString();
        let num = trabajos.length ? trabajos.sort((a,b) => a.num - b.num)[trabajos.length-1].num : 0;
        let trabajo = {
            id: _uuid.v4(),
            Bimestre: _bimestre, 
            IDGrupo: _grupo,
            FechaSync: fechaSync,
            num: ++num,
            actividad: actividad || '',
            nombre: `Trabajo-${num}`,
            fecha: `${fecha.getDate()}-${fecha.getMonth() + 1}-${fecha.getFullYear()}`,
            entrega: __smtdb__.alumnos
                .filter(a => a.IDGrupo == _grupo && a.Deleted !== true)
                .map(o => ({
                    id: o.IDAlumno,
                    estado: estado, 
                    FechaSync: fechaSync
                }))
        };

        trabajos.push(trabajo);
        _Logued.set(__smtdb__).then(() => 
            _a.generarTrabajo(selector, trabajo, _grupo, true, true));
    }

    this.editar = function (id, fecha, observacion, nombre, tipo, actividad) {
        var sess = __smtdb__.trabajos.filter(s => s.id == id);
         
        if (sess.length) {
            sess = sess[0];
            sess.fecha = fecha.replace(/\//g, '-');
            sess.observacion = observacion;
            sess.FechaSync = (new Date()).toISOString();
            sess.nombre = nombre;
            sess.tipo = tipo;
            sess.actividad = actividad;
        }
       
        _Logued.set(__smtdb__).then(() => {
            $('[data-trabajo-id="' + id + '"]').attr('data-trabajo-fecha', fecha).resaltar('info', 800);
            Trabajo.desplegarResultados(_grupo, '#tabla-trabajo');

        });
    }

    this.eliminar = function (id) {
        ConfirmDialog.show({
            title: 'Eliminar Trabajo',
            text: '<h3 class="text-center">Esta intentando eliminar un trabajo permanentemente, el cual ya no se podrá recuperar. ¿Desea continuar?</h3>',
            positiveButtonClass: 'btn btn-danger',
            positiveButtonText: 'Si',
            negativeButtonClass: 'btn btn-success',
            negativeButtonText: 'No',
            closeModalOnAction: false,
            callback: result => {
                if (result == true) {
                    var trab = __smtdb__.trabajos.filter(s => s.id == id);

                    if (trab.length) {
                        trab = trab[0];
                        trab.FechaSync = (new Date()).toISOString();
                        trab.Deleted = true;

                        _Logued.set(__smtdb__).then(() => {
                            $('[data-trabajo-id="' + id + '"]').removeConEfecto();
                            AlertSuccess('Se ha eliminado el trabajo', 'Trabajos');
                           
                            ConfirmDialog.hide();
                            // actualizaDataEnCache();
                        })
                    }
                }
                else {
                    ConfirmDialog.hide();
                }
            }
        });

    }

    this.modaleditar = function(id) {
        Loading("Cargando");
        $.ajax({
            type: 'GET',
            url: '/Trabajos/CargarTrabajo',
            data: { ID: id }
        })
        .done(function (data) {
            $('#formularioTrabajo .modal-body').empty(data);
            $('#formularioTrabajo .modal-body').append(data);
            Loading();
            $('#formularioTrabajo').modal("show")
        });
    }

    this.guardar = function () {
        var form = $("#formularioTrabajo form");
        if (form.valid()) {
            $.ajax({
                url: '/Trabajos/GuardarTrabajo',
                data: form.serializeArray(),
                method: "POST",
                beforeSend: function () {
                    Loading('Guardando');
                },
                success: function (data) {
                    Loading();
                    if (data > 0) {
                        AlertSuccess('Se ha guardado el registro')
                        $('#formularioTrabajo').modal("hide");                        
                    } else {
                        AlertError('No se ha podido guardar: ' + data);

                    }
                }
            })
        }
    }

    // Generar select para la captura
    $('body:not(.visualizando)').delegate('[data-alumno-id][data-alumno-trabajo-estado]', 'focusin', function () {
        var td = $(this);
        if (td.find('select').length == 0) {
            let template = _require('../app/bimestre/templates/capturaTrabajo.js')
            td.empty();
            $(template()).appendTo(td)
                .focus(function () {
                    // Esto hace que se carge el select abierto.. no encontre otra manera
                    $(this).attr('size', $(this).attr("expandto"));
                })
                .focus();
        }
    });

    // Reestablecer el td para mostrar unicamente el texto del estado
    $('body:not(.visualizando)').delegate('[data-alumno-id][data-alumno-trabajo-estado] select', 'focusout', function () {

        var estado = $(this).parent('div').attr('data-alumno-trabajo-estado');
        $(this).parent('div').html(obtenerHtmlEstado(estado));
    });

    // Cambiar el estado inmediatamente al seleccionar opcion
    $('body:not(.visualizando)').delegate('[data-alumno-id][data-alumno-trabajo-estado] select', 'change', function () {

        if ($(this).val() != '') {
            var alumno = $(this).parent('div').attr('data-alumno-id');
            var sesion = $(this).parents('tr').attr('data-trabajo-id');
            var grupo = $(this).parents('tr').attr('data-grupo');
            actualizarAsistencia(alumno, sesion, $(this).val(), grupo);
        }
        else
            $(this).focusout();
    });

    $('body:not(.visualizando)').delegate('[data-trabajo-id] [name="observacion"],[data-trabajo-id] [name="fecha"],[data-trabajo-id] [name="nombre"],[data-trabajo-id] [name="tipo"],[data-trabajo-id] [name="actividad"]', 'change', function () {
        var tr = $(this).parents('[data-trabajo-id]');

        _a.editar(tr.attr('data-trabajo-id'), tr.find('[name="fecha"]').val(), tr.find('[name="observacion"]').val(), tr.find('[name="nombre"]').val(), tr.find('[name="tipo"]').val(), tr.find('[name="actividad"]').val());
    });


    $('body:not(.visualizando)').delegate('[data-trabajo-id] [data-asitencia-option="eliminar"]', 'click', function () {
        var tr = $(this).parents('[data-trabajo-id]');
        _a.eliminar(tr.attr('data-trabajo-id'));
    });
    $('body:not(.visualizando)').delegate('[data-trabajo-id] [data-trabajo-option="editardescripcion"]', 'click', function () {
        var tr = $(this).parents('[data-trabajo-id]');
        _a.modaleditar(tr.attr('data-trabajo-id'));
    });

    this.Imprimir = function () {
        let url = __server__ + '/trabajos/imprimir?grupo=' + _grupo + '&bimestre=' + _bimestre;
        let download = _Downloader.download(url);
        Loading("...")
        download.then(res => {
            Loading();
            open(res.file); 
        }).catch(err => {
            console.error(err)

            _Downloader.cached(url).then(res => {
                Loading();
                open(res.file);
                AlertError("Mostrando versión desactualizada, conectese a internet para ver una versión actualizada")
            }).catch(err => {
                AlertError("Conectese a internet para descargar la version para imprimir")
                Loading();
                console.error(err)
            })
        })
    }

    this.exportar = function () {

        // Se clona porque se va a editar el html para transformar los inputs en texto
        var tabla = $('#tabla-trabajo table').clone();

        tabla.find('td').each(function () {
            var td = $(this);
            td.find('input,textarea').each(function(){
                td.append($(this).val());
                $(this).remove();
            });

            td.find('.fa-check, .fa-close').each(function () {
                if ($(this).hasClass('fa-check'))
                    td.append(String.fromCharCode(10003));
                else
                    td.append(String.fromCharCode(10005));
                $(this).remove();
            });
        });

        tableToExcel(tabla[0], 'Trabajos');
    }

 
}

$('body:not(.visualizando)').delegate('[data-trabajo="nuevo"]', 'click', function () {
    Trabajo.nuevo('#tabla-trabajo', 1);
});

$('body:not(.visualizando)').delegate('[data-trabajo="suspencion"]', 'click', function () {
    Trabajo.nuevo('#tabla-trabajo', 3);
});