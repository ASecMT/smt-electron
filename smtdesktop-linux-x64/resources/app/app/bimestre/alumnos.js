/**
 * bimestre.html
 * - Bimestre y todas las secciones
 */

var CerrarSesion = () => {
    $.confirm({
        title: 'Desligar cuenta',
        content: "¿Estás seguro que deseas salir? en caso de no haber sincronizado tus datos, se perderán",
        confirmButton: 'SI',
        confirmButtonClass: 'btn-info',
        cancelButton: 'NO',
        icon: 'fa fa-question-circle',
        animation: 'scale',
        confirm: () => {
            window._iPCRenderer.send("logout");
        }
    });
};
function Refrescar(){
window.location.reload();
}

var Sincronizar = (silent) => {
    if (silent !== true) {
        Loading("Sincronizando");
     
    }
    
    console.groupCollapsed(`[${Date.now()}] Attempt to sync`);
    console.info('Sync started');

    return _Sync.all($, __smtdb__, __server__).then(smtdb => {
        window.__smtdb__.grupos = smtdb.grupos;
        window.__smtdb__.reporte = smtdb.reporte;
        window.__smtdb__.alumnos = smtdb.alumnos;
        window.__smtdb__.sesiones = smtdb.sesiones;
        window.__smtdb__.trabajos = smtdb.trabajos;
        window.__smtdb__.examenes = smtdb.examenes;
        window.__smtdb__.portafolio = smtdb.portafolio;
        window.__smtdb__.habilidades = smtdb.habilidades;
        window.__smtdb__.instrumentos = smtdb.instrumentos;
        window.__smtdb__.tipoPortafolio = smtdb.tipoPortafolio;
        window.__smtdb__.diagnosticoCiclo = smtdb.diagnosticoCiclo;

        window.__alumnos__ = window.__smtdb__.alumnos;
        Alumnos.desplegarLista(_grupo, '[data-alumno-lista]');

        if(silent !== true) {
            AlertSuccess("Se sincronizó correctamente");
            Loading();
            Refrescar();
        }

        console.info('Successfully synchronized');
        console.groupEnd();
    }).catch(err => {
        if(silent !== true) {
            AlertError((err || {}).message || "Ocurrió un problema");
            Loading();
           
        }
        
        console.error((err || {}).message || "Error with sync request");
        console.groupEnd();
        
    })
};





var Alumnos = new function () {
    var _a = this;

    this.data = [];
    this.limpiar = function (grupo) {
        Cache.vaciar();
        _a.data = [];
    }

    this.listar = grupo => __smtdb_promise__.then(smtdb => smtdb.alumnos.filter(a => a.IDGrupo == grupo && a.Deleted !== true))
    this.desplegarLista = function (grupo, selector) {
        const RowAlumnoTallerTemplate = _require('../app/bimestre/templates/rowAlumnoTaller.js');
        const rowAlumnoTemplate = _require('../app/bimestre/templates/rowAlumno.js');
        const template = $("#GrupoEsTaller").val() == "true" ? RowAlumnoTallerTemplate : rowAlumnoTemplate;
        
        let data = __alumnos__.filter(a => a.IDGrupo == _grupo && a.Deleted !== true);
        Alumnos.data = __alumnos__.filter(a => a.IDGrupo == _grupo && a.Deleted !== true);

        if (data.length == 0) {
            $(selector).html('<tr><td class="text-center" colspan="5">No se encontraron alumnos</td></tr>');
            if (Alumnos.data.length == 0) {
                $('#tabs a:not(:first), #btnPerfil,#btnImprimirAlumnos').addClass('hide');
                AlertInfo('Este grupo no cuenta con alumnos registrados, para que se muestren todas las opciones debes capturar a los alumnos del grupo');
                $('[data-alumno="nuevo"]').popover({
                    title: 'Alumnos',
                    content: 'Da clic para crear un alumno en el grupo',
                    trigger: 'manual',
                    placement: 'top'
                }).popover('show');

                setTimeout(function () {
                    $('[data-alumno="nuevo"]').popover('hide');
                },5000);
                return;
            }
        }
        else {
            $('#tabs a, #btnPerfil,#btnImprimirAlumnos').removeClass('hide');
            $(selector).empty();
            var estados = ['#f5caf5', '#c0d9b7', '#f0ad4e', '#d9534f'];
            for (var i = 0; i < data.length; i++) {
                var alum = data[i];

                alum.num = i + 1;
                alum.color = alum.colorSemaforo;

                $(selector).append(template(alum));
            }
        }
    }

    const cargarAlumnoTemplate = _require('../app/bimestre/templates/cargarAlumno.js');
    
    this.cargarAlumno = function (ID) {
        let alumno = {
            IDAlumno: _uuid.v4(),
            IDGrupo: __grupo__.IDGrupo,
            EsTaller: $("#GrupoEsTaller").val() !== "false"
        }

        if(__alumnos__ && __alumnos__.length) {
            let query = __alumnos__.filter(a => a.IDAlumno == ID);
            alumno = query.length ? query[0] : alumno;
        }

        $('#formularioAlumno .modal-body').empty();
        $('#formularioAlumno .modal-body').append(cargarAlumnoTemplate(alumno));
        $('#formularioAlumno').modal("show");
    }

    this.guardarAlumno = function () {
        let form = $("#formularioAlumno form");
        if (form.valid()) {
            let id = form.find("[name=IDAlumno]").val();
            let alumnos = window.__smtdb__.alumnos;
            let alumno = alumnos.filter(a => a.IDAlumno == id)
            let serializedForm = form.serializeArray();

            if(alumno.length) {
                alumno = alumno[0]
            } else {
                alumno = {
                    Semaforo: 'BIEN',
                    ColorSemaforo: '#D8FDD1'
                }
                alumnos.push(alumno);
            }

            alumno.FechaSync = (new Date()).toISOString();
            serializedForm.forEach(i => alumno[i.name] = i.value);

            _Logued.set(window.__smtdb__);

            __alumnos__ = __smtdb__.alumnos;

            AlertSuccess('Se ha guardado el registro')
            $('#formularioAlumno').modal("hide");
            Alumnos.limpiar(_grupo)                        
            Alumnos.desplegarLista(_grupo, '[data-alumno-lista]');
            Dropzone.forElement("#dropzone").removeAllFiles(true);
            $('#formularioAlumno').modal("hide");
        }
    }

    this.eliminarAlumno = function (ID) {
        $.confirm({
            title: 'Alumnos',
            content: "¿Esta seguro de querer eliminarlo?",
            confirmButton: 'Aceptar',
            confirmButtonClass: 'btn-info',
            cancelButton: 'Cancelar',
            icon: 'fa fa-question-circle',
            animation: 'scale',
            confirm: function () {
                var index = __smtdb__.alumnos.findIndex(a => a.IDAlumno == ID);
                __smtdb__.alumnos[index]["Deleted"] = true;
                __smtdb__.alumnos[index]["FechaSync"] = (new Date()).toISOString();
                _Logued.set(__smtdb__).then(() => {
                    AlertSuccess("Se ha eliminado correctamente");                            
                    Alumnos.limpiar(_grupo)
                    Alumnos.desplegarLista(_grupo, '[data-alumno-lista]');
                })
            }
        });
    }

    this.asignarTutor = function (id) {
        var $form;
        var $modal;
        var fetchTutores = function () {
            return new Promise(function (ok, no) {
                $.post(__server__ + 'Tutor/ListarTutores?idAlumno=' + id, function (res) {
                    if (res.result) ok(res.data);
                    else {
                        no("Ocurrió un problema al cargar los tutores");
                        console.error(res.message);
                    }
                }).error(xhr => no("No se pudo conectar al servidor"));
            });
        };
        var tutores = fetchTutores();
        var removeTutor = function () {
            var $me = $(this)
            var $row = $me.parents("[data-id]");
            var idTutor = $row.data("id")
            var data = { idTutor, idAlumno: id };

            Loading("...");
            $.post(__server__ + 'Tutor/QuitarAlumno', data, function (res) {
                if (res.result) {
                    $row.remove(); 
                    AlertSuccess("Se quitó el tutor");
                    if (!$modal.find("[data-id]").length) updateTable(Promise.resolve([]));
                }
                else AlertError(res.message);
            }).fail(function (r) {
                AlertError("Ocurrió un problema al quitar el tutor");
            }).always(function () { Loading() })
        }

        var updateTable = function (promise) {
            var info =
                $modal.find(".info").length ? $modal.find(".info") :
                $('<div class="info alert alert-info text-center" role="alert"></div>')
                    .appendTo($modal.find("[data-tutores]"));

            promise.catch(function (err) {
                info.removeClass("alert-info").addClass("alert-danger");
                info.html('<i class="fa fa-close"></i> ' + err);
            });
            promise.then(function (data) {
                if (!data.length) return info
                    .html('<i class="fa fa-info-circle"></i> sin tutores');

                $modal.find('[data-tutores]').html(data.map(function (tutor) {
                    var html = '<div data-id="{id}">{label} <i class="fa fa-close pull-right"></i></div>';
                    var obj = { id: tutor.Id, label: tutor.Nombre.bold() + ' - ' + tutor.Email };
                    return html.format(obj);
                }));

                $modal.find('[data-tutores] [data-id] .fa-close').click(removeTutor);
            });
            return promise;
        };

        var prepareForm = function () {
            $form = $modal.find("form");
            $form.find('[name=Id]').val(id);

            $.validator.unobtrusive.parse($form);
            updateTable(tutores);

            $form.submit(function (ev) {
                if (!$form.valid()) return false;
                ev.preventDefault();
                Loading("Enviando...");
                $.post($form.attr("action"), $form.serializeArray(), function (res) {
                    if (res.result) {
                        AlertSuccess("Se agregó correctamente");
                        updateTable(fetchTutores()).then(function () { Loading() });
                        $form.find('[name=Email]').val("");
                    } else { Loading(); AlertError(res.message); }
                }).fail(function () {
                    AlertError("No se pudo comunicar con el servidor");
                    Loading();
                })
            });
        }

        var dialogcfg = {
            title: 'Asignar tutor',
            closeModalOnAction: false,
            positiveButtonText: "Agregar",
            negativeButtonText: "Cerrar",
            callback: function (result) {
                if (result == true) $form.submit();
                else ConfirmDialog.hide();
            },
            beforeOpen: function ($m) { $modal = $m; prepareForm() }
        };

        Loading("Cargando formulario");
        Templates.load('asignarTutorAlumno', __server__ + 'Tutor/AgregarAlumno').then(function (template) {
            Loading();
            ConfirmDialog.show($.extend(dialogcfg, { text: template }));
        }).catch(() => AlertError("No se pudo comunicar con el servidor"));
    }

    this.modalImportar = function () {        
        $("#formularioimportarAlumno").modal("show");
    }

    this.descargarPlantilla = () => {
        Loading("Descargando");

        let outFile = _path.join(__data__, 'importarAlumnos.xlsx');
        let plantilla = __server__ + '/Alumnos/DescargarTemplateAlumnos';

        let choosePathDialog = new Promise((ok, no) => {
            let dialog = _require("electron").remote.dialog;
            dialog.showSaveDialog({
                defaultPath: 'importarAlumnos.xlsx'
            }, path => {
                if(path) ok(path);
                else no();
            });
        });

        let download = () => new Promise((ok, no) => {
            let out = _fs.createWriteStream(outFile);
            let cookie = __smtdb__.cookie;
            let req = _request({
                method: 'GET',
                uri: plantilla,
                headers: {
                    'Cookie': `${cookie.name}=${cookie.value}`
                }
            });

            req.on('response', data => {
                let content = data.headers["content-disposition"];
                if (content != "attachment;filename=Plantilla.xlsx") {
                    no(Error("Ocurrió un error al descargar el archivo"));
                }
            });

            req.on('end', () => ok(outFile));
            req.pipe(out);
        });

        return choosePathDialog
            .then(path => {
                outFile = path;

                return download().then(() => {
                        Loading("Abriendo");
                        _shell.openItem(outFile);
                        Loading();
                    }).catch(error => {
                        Loading();
                        console.error(error);
                        AlertError(error.message || "Ocurrió un error");
                    });
            }).catch(error => {
                console.info("user cancelled download");
                Loading();
            });
    };

    this.importarAlumno = function () {
        if (navigator.onLine) {

        var form = $("#formularioimportarAlumno form")
        var files = form.find('.dropzone')[0].dropzone.getAcceptedFiles();
        var formData = new FormData();
        
        formData.append("IDGrupo", _grupo);
        formData.append("Bimestre", _bimestre);

        $.each(files, function (i, v) {
            formData.append('archivo', v);
        });

         $.ajax({
            type: 'POST',
            url: __server__ + 'Alumnos/ImportarAlumnos',
            data: formData,
            processData: false,
            contentType: false,
            xhr: function () {
                myXhr = $.ajaxSettings.xhr();

                return myXhr;
            },
            beforeSend: () => Loading("Importando"),
            success: data => {
                if (data > 0) {
                    $("#formularioimportarAlumno").modal("hide");
                    AlertSuccess('Se ha guardado la exitosamente.');
                    Loading("Sincronizando");
                    Sincronizar()
                        .then(() => window.location.reload())
                        .catch(() => AlertError("No se pudo importar"));
                } else {
                    AlertError(data);
                }
            },
            error: () => AlertError('No se pudo guardar. Intente nuevamente.', '')
        });

        }else{
            AlertWarning("Esta acción requiere conexion a internet")
        }
    }
  
    this.cargarSemaforo = function (seccion, alumno) {
        let semaforo = __smtdb__.semaforo.filter(s => 
            s.IDGrupo == _grupo && 
            s.Bimestre == _bimestre &&  
            (alumno ? s.id == alumno : true));
        
        if (!alumno) {
            let notExists = __alumnos__
                .filter(a => !semaforo.filter(s => s.id == a.IDAlumno).length)
                .map(a => {
                    let index = __smtdb__.semaforo.findIndex(s => s.id == a.IDAlumno);
                    let color = index >= 0 ? __smtdb__.semaforo[index].colorPromedio : "#000000";
                    let name = seccion == "diagnostico-ciclo" ? "diagnosticoCiclo" : seccion;
                    let result = {};

                    result.id = a.IDAlumno;
                    result[name] = color;

                    return result;
                });

            semaforo = semaforo.concat(notExists);
        }

        if (semaforo.length) {
            semaforo.forEach(a => {
                let color = a[seccion == "diagnostico-ciclo" ? "diagnosticoCiclo" : seccion];
                $('#' + (seccion == "portafolio" ? "instrumentos" : seccion))
                    .find('[data-alumno="' + a.id + '"].semaforo')
                    .css('background-color', color || a["colorPromedio"]);
            })
        }
    }

    this.imprimir = function () {
        Loading("...");

        let url = __server__ + '/alumnos/imprimir?grupo=' + _grupo;

        _Downloader.download(url).then(res => {
            Loading();
            open(res.file); 
        }).catch(err => {
            console.error(err);

            _Downloader.cached(url).then(res => {
                Loading();
                open(res.file);
                AlertError("Mostrando versión desactualizada, conectese a internet para ver una versión actualizada");
            }).catch(err => {
                AlertError("Conectese a internet para descargar la version para imprimir");
                Loading();
                console.error(err)
            })
        })
    }
}


$('body').delegate('[data-alumno="editar"]', 'click', function (e) {
    e.preventDefault();

    
});

$('body').delegate('[data-alumno="eliminar"]', 'click', function (e) {
    e.preventDefault();

});