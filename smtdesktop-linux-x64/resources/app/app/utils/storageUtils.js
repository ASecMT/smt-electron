/**
 * Este módulo esta diseñado para correr tanto en 
 * mainProcess como en rendererProcess. Sirve para 
 * utilizar las apis del módulo npm 'electron-json-storage'
 * a través de las promesas de javascript 
 */

const req = typeof require == "undefined" ? _require : require;
const storage = req('electron-json-storage');
const localStorage = {
  get: key => new Promise((ok, no) => {
    storage.get(key, (e, r) => {
      if(e) no(e)
      else ok(r)
    })
  }),
  set: (key, val) => new Promise((ok, no) => {
    storage.set(key, val, e => {
      if(e) no(e)
      else ok()
    })
  }),
  remove: key => new Promise((ok, no) => {
    storage.remove(key, e => {
      if(e) no(e)
      else ok()
    })
  })
}

module.exports = { 
  localStorage
}