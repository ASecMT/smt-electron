﻿/**
 * Este módulo se carga antes del html en 
 * el rendererProcess y sirve para inyectar las
 * APIs de nodejs como variables globales con diferente
 * nombre en el objeto window
 */
const _electron = require('electron');
const _url = require('./url/url.js');
const _path = require('path');

const _downloader = require('./utils/downloadHTML.js');

const __path__ = _path.join(__dirname, '..');
const _iPCRenderer = _electron.ipcRenderer;
const _require = require;
const _process = process;

const lazify = require('./utils/lazify.js');

process.once('loaded', () => {
    window._iPCRenderer = _iPCRenderer;
    window._require = _require;
    window.__path__ = __path__;
    window.__url__ = 'file://' + __path__ + '/';

    lazify(window, "_uuid", () => _require('node-uuid'));
    lazify(window, "_storage", () => _require('electron-json-storage'));
    lazify(window, "_fs", () => _require('fs'));
    lazify(window, "_path", () => _require('path'));
    lazify(window, "_request", () => _require('request'));
    lazify(window, "_shell", () => _electron.shell);

    lazify(window, "_Logued", () => _require('./storage/Logued.js'));
    lazify(window, "_Navigate", () => _require('./storage/Navigate.js'));
    lazify(window, "_Sync", () => _require('./sync/sync.js'));
    lazify(window, "_Downloader", () => _downloader);

    window.__data__ = _electron.remote.app.getPath("userData");
    window.__server__ = _url.server;

    _iPCRenderer.send('SMT_INTERNAL_WINDOW_CREATED', 200);
    _iPCRenderer.on('SMT_INTERNAL_BACKGROUND_SYNC', (channel, listener) => {
        if (typeof Sincronizar !== "undefined") {
            console.info(`[${Date.now()}] Received signal to sync`);
            Sincronizar(true);
        } else {
            console.info(`[${Date.now()}] Couldn't sync`);
        }
    });
});