/**
 * Este modulo descarga el html de una pagina completa
 * y la guarda en un cache, hace las peticiones
 * utilizando la Auth Cookie de ASP.NET  
 */

const req = typeof require == "undefined" ? _require : require;
const MD5 = req('js-md5');
const fs = req('fs');
const path = req('path');
const electron = req('electron');
const scraper = req('website-scraper');

const appData = electron.remote.app.getPath("userData");
const getAuthCookie = () => {
    return new Promise((ok, no) => {
        _Logued.get().then(db => {
            let cookie = db["cookie"];
            if(cookie) ok(db["cookie"]);
            else no(new Error("No existe la cookie"));
        }).catch(no)
    })
}

const download = (url) => {
    const folder = path.join(appData, `/${MD5(url)}-${Date.now()}/`);
    const cookie = getAuthCookie();

    return new Promise((ok, no) => {
        cookie.then(c => {
            var options = {
                urls: [url],
                directory: folder,
                request: {
                    headers: {
                        "Cookie": c.name + "=" + c.value
                    }
                }
            };

            scraper.scrape(options, (error, result) => {
                if(error) {
                    fs.rmdir(folder, (err) => {
                        if (err) console.error(err);
                        else console.info("cache directory removed");

                        return no(error);
                    });
                }
                else return ok({file: path.join(folder, "index.html")});
            });  
        }).catch(no);
    });
}

const cached = (url) => {
    const hash = MD5(url);
    return new Promise((ok, no) => {
        fs.readdir(appData, function(err, items) {
            if(err) return no(err);

            items = items
                .filter(item => item.startsWith(hash))
                .map(item => item.split('-'))
                .sort((a, b) => ~~a[1] < ~~b[1])
                .map(item => item.join('-'));

            if (items.length) {
                ok({file: path.join(appData, `/${items[0]}/`, 'index.html')})
            }
            else {
                no(new Error("No cached data"))
            }
        });
    })
}

module.exports = { download, cached }