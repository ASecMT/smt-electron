module.exports = model => `

<li class="col-md-6 col-sm-6 col-xs-6">
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>${model.index}. ${model.nombre}</th>
                <th>${model.GradoGrupo}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Trabajos y Tareas que Cumplió</td>
                <td>${model.totalTrabajosCumplidos}</td>
            </tr>
            <tr>
                <td>Trabajos y Tareas medios</td>
                <td>${model.totalTrabajosMedios}</td>
            </tr>
            <tr>
                <td>Trabajos y Tareas que no Cumplió</td>
                <td>${model.totalTrabajoNoCumplidos}</td>
            </tr>
            <tr data-mostrar="promedioEsquema" class="hide">
                <td>Esquema y Mapas Conceptuales</td>
                <td class="calificacion">${model.promedioEsquema}</td>
            </tr>
            <tr data-mostrar="promedioEnsayo"  class=" hide"">
                <td>Ensayo</td>
                <td class="calificacion">${model.promedioEnsayo}</td>
            </tr>
            <tr data-mostrar="promedioExposicion"  class="hide"">
                <td>Exposición</td>
                <td class="calificacion">${model.promedioExposicion}</td>
            </tr>
            <tr data-mostrar="promedioGuia"  class="hide">
                <td>Guía de Observación</td>
                <td class="calificacion">${model.promedioGuia}</td>
            </tr>
            <tr data-mostrar="promedioListaCotejo"  class="hide">
                <td>Lista de Cotejo o Control</td>
                <td class="calificacion">${model.promedioListaCotejo}</td>
            </tr>
            <tr data-mostrar="promedioLineaTiempo"  class="hide">
                <td>Línea de tiempo</td>
                <td class="calificacion">${model.promedioLineaTiempo}</td>
            </tr>
            <tr data-mostrar="promedioMapaAprendisaje"  class="hide">
                <td>Mapa de Aprendizaje</td>
                <td class="calificacion">${model.promedioMapaAprendisaje}</td>
            </tr>
            <tr data-mostrar="promedioPortafolio"  class="hide">
                <td>Portafolio</td>
                <td class="calificacion">${model.promedioPortafolio}</td>
            </tr>
            <tr data-mostrar="promedioProduccionesEscritas"  class="hide">
                <td>Producciones Escritas o Gráficas</td>
                <td class="calificacion">${model.promedioProduccionesEscritas}</td>
            </tr>
            <tr data-mostrar="promedioProyecto"  class="hide">
                <td>Proyecto</td>
                <td class="calificacion">${model.promedioProyecto}</td>
            </tr>
            <tr data-mostrar="promedioRegistroAnecdotico"  class="hide">
                <td>Registro Anecdótico o Anecdotario</td>
                <td class="calificacion">${model.promedioRegistroAnecdotico}</td>
            </tr>
            <tr data-mostrar="promedioRevisionCuadernos"  class="hide">
                <td>Revisión de Cuadernos</td>
                <td class="calificacion">${model.promedioRevisionCuadernos}</td>
            </tr>
            <tr data-mostrar="promedioRubrica"  class="hide">
                <td>Rúbrica o Matriz de Verificación</td>
                <td class="calificacion">${model.promedioRubrica}</td>
            </tr>
            <tr>
                <td>Examen Parcial</td>
                <td class="calificacion">${model.promedioExamenParcial}</td>
            </tr>
            <tr>
                <td>Examen Bimestral</td>
                <td class="calificacion">${model.promedioExamenBimestral}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="2">${model.Materia}</th>
            </tr>
        </tfoot>
    </table>
</li>

`