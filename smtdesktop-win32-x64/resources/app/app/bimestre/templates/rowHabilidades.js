module.exports = model => `

<tr data-grupo="${model.grupo}" data-habilidades-fecha="${model.fecha}" data-trabajo-id="${model.id}" class="resaltar">
    <td>${model.nombre}</td>    
    ${model.tds}
    <td data-habilidades-total="${model.id}">0</td>
    <td data-habilidades-promedio="${model.id}">0</td>    
</tr>

`