/**
 * Este módulo se carga mediante un <script>
 * y define algunas variables globales para que 
 * funcione correctamente la vista de Bimestre.
 */
const __smtdb_promise__ = _Logued.get();
const __navigate_promise__ = new Promise((ok, no) => {
	__smtdb_promise__
		.then(smtdb => _Navigate.get()
			.then(navigate => ok(__setup_vars__(smtdb, navigate))))
		.catch(e => no(e));
})

const __setup_vars__ = function(smtdb, navigate) {
	let grupos = smtdb.grupos;
	let alumnos = smtdb.alumnos;
	let grupo = [];
	let alumnosGrupo = [];

	if(navigate && navigate.hasOwnProperty("IDGrupo") && grupos && grupos.length) {
		grupo = grupos.filter(g => g["IDGrupo"] == navigate["IDGrupo"]);
		grupo = grupo && grupo.length ? grupo[0] : null;
	}

	if (grupo && alumnos && alumnos.length) {
		alumnosGrupo = alumnos.filter(a => a["IDGrupo"] == grupo["IDGrupo"]);
	}

	return { grupo, alumnos: alumnosGrupo, navigate };
}

__smtdb_promise__.then(smtdb => {
	window.__smtdb__ = smtdb;
})

__navigate_promise__.then(data => {
	window.__grupo__ = data.grupo;
	window.__alumnos__ = data.alumnos;
})