/**
 * Punto de entrada para electron.
 * Este corre directamente en NodeJS 
 */
const storage = require('electron-json-storage');
const electron = require('electron');
const path = require('path');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const Logued = require('./app/storage/Logued.js');

/** @type {Electron.BrowserWindow} */
let mainWindow = null;

const crearVentana = (url, width, height, resizable) => {
  let win = new BrowserWindow({
    webPreferences: {
      nodeIntegration: false,
      preload: path.join(__dirname, 'app', 'preloadNode.js'),
    },
     icon: __dirname + '/images/favicon.png',
    width: width || 1200,
    height: height || 900,
    toolbar: false,
    menubar: false,
    resizable: resizable || true

  });

  win.loadURL(url);
  return win;
}

class Utils {
    /**
     * @param {Electron.BrowserWindow} cWindow
     * @return {Promise<Electron.Cookie>}
     */
    static getCookieFromSession(cWindow) {
        var session = cWindow.webContents.session;
        return new Promise((ok, no) => {
            session.cookies.get({}, (error, cookies) => {
                if(cookies && cookies.length) {
                    var authCookie = cookies.filter(c => c.name == ".AspNet.ApplicationCookie");
                    var cookie = authCookie.length ? authCookie[0] : null;
                    if (cookie) {
                        ok(cookie);
                    } else {
                        no(new Error("No cookie found"))
                    }
                }
                if(error) no(error)
            })
        })
    }
}

class Login {
    static createWindow() {
        let window = crearVentana("file://" + __dirname + "/html/login.html", 1100, 690, false);
        let logued = false;
        let promise = new Promise((ok, no) => {
            window.on('closed', () => {
                if(!logued) no()
            });
            electron.ipcMain.once('login', (channel, listener) => {
                logued = true;
                ok({channel, listener});
            });
        });

        return {window, promise};
    }

    static saveCookie() {
        return Logued.get().then(db => {
            return Utils.getCookieFromSession(mainWindow).then(cookie => {
                cookie["url"] = "http://localhost:35177/";
                db["cookie"] = cookie;
                db["is"] = true;

                return Logued.set(db);
            })
        });
    }

    static attemptToLogin() {
        let login = Login.createWindow();

        mainWindow = login.window;
        login.promise
            .then(() => Login.saveCookie())
            .then(() => Main.createWindow(login.window))
            .catch(() => mainWindow = null);
    }
}

class Main {
    static createWindow(loginWindow, cookie) {
        let gruposWindow = crearVentana("file://" + __dirname + "/html/grupos.html");

        if (loginWindow) {
            loginWindow.on('closed', () => {
                mainWindow = gruposWindow;

                Main.addIPCLogoutListener(gruposWindow);
                Main.startSyncDaemon();
            });
        } else {
            mainWindow = gruposWindow;
            gruposWindow.webContents.session.cookies.set(cookie, error => {
                if (error) console.error(error);
            });

            Main.addIPCLogoutListener(gruposWindow);
            Main.startSyncDaemon();
        }

        gruposWindow.on('closed', () => {
            mainWindow = null;
        });

        if (loginWindow) {
            loginWindow.close();
        }
    }

    static startSyncDaemon() {
        let daemon = channel => () => {
            channel.sender.send('SMT_INTERNAL_BACKGROUND_SYNC');
        };

        electron.ipcMain.on('SMT_INTERNAL_WINDOW_CREATED', (channel, listener) => {
            let daemonId = `window-${channel.sender.id}`;
            let windows = Object.keys(Main.syncDaemonInterval); 

            if (!Object.hasOwnProperty(daemonId, Main.syncDaemonInterval) && windows.length == 0) {
                // cada 5 min
                Main.syncDaemonInterval[daemonId] = setInterval(daemon(channel), 1000 * 60 * 5);
            }
        });
    }

    static killSyncDaemon() {
        let windows = Object.keys(Main.syncDaemonInterval);
        let i = windows.length;
        
        while(--i >= 0) {
            clearInterval(Main.syncDaemonInterval[windows[i]]);
        }
    }

    static addIPCLogoutListener(currentWindow) {
        electron.ipcMain.once('logout', (channel, listener) => {
            Logued.remove().then(() => currentWindow.close());
            Main.killSyncDaemon();
            relaunch();
        });
    }
}

Main.syncDaemonInterval = {};

function createWindow() {
  Logued.get().then(db => {
    let isLoguedIn = !!db && !!db['is'];

    if (!isLoguedIn) Login.attemptToLogin();
    else Main.createWindow(undefined, db["cookie"]);
  });
}

app.on('ready', createWindow);
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});



function relaunch(){
    app.relaunch();
}