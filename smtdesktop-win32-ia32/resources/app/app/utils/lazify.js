/**
 * Este metodo hace que al acceder a una variable
 * se ejecute el metodo "instantiation()" y cachear
 * el resultado de su ejecucion para regresarlo en
 * cada vez que se acceda a la misma variable.
 */
const lazify = (obj, name, instantiation, readOnly = true) => {
    let instance;

    Object.defineProperty(obj, name, {
        get: () => // "void 0" same as "undefined"
            instance === void 0 ? (instance = instantiation()) : instance,
        configurable: false,
        writtable: !readOnly
    });
}

module.exports = lazify;