/**
 * A través de este módulo se accesa a la base de datos
 * de la app. Se almacena en el archivo logued.json
 */

const req = typeof require == "undefined" ? _require : require;
const localStorage = req('../utils/storageUtils.js').localStorage;

/**
 * SMT Section
 * @typedef {Object} SMTSeccion
 */

/**
 * SMT User type definition
 * @typedef {Object} SMTUser
 * @property {string} id - User id.
 * @property {string} name - User name.
 * @property {SMTSeccion} secciones - Indicates which sections are available for this user.
 */

/**
 * The complete SMT database
 * @typedef {Object} SMTDB
 * @property {boolean} is
 * @property {SMTUser} user
 * @property {Object} grupos
 * @property {Electron.Cookie} cookie
 * @property {Object} alumnos
 * @property {Object} reporte
 * @property {Object} sesiones
 * @property {Object} trabajos
 * @property {Object} examenes
 * @property {Object} semaforo
 * @property {Object} habilidades
 * @property {Object} instrumentos
 * @property {Object} tipoPortafolio
 * @property {Object} diagnosticoCiclo
 */

const Logued = {
  /**
   * Reads the whole local SMT Database
   * @return {Promise<SMTDB>}
   */
  get: () => localStorage.get("logued"),
  /**
   * Writes the whole local SMT Database
   * @param {SMTDB} val
   * @return {Promise<SMTDB>}
   */
  set: val => localStorage.set("logued", val),
  /**
   * Removes the whole local SMT Database
   * @return {Promise<void>}
   */
  remove: () => localStorage.remove("logued"),
}

module.exports = Logued;