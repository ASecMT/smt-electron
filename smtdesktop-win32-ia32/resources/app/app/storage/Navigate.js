/**
 * Se utiliza para comunicar información entre un 
 * archivo html y otro.
 * 
 * TODO: Tal vez sería mas eficiente enviar los parametros por URL
 */
const req = typeof require == "undefined" ? _require : require;
const localStorage = req('../utils/storageUtils.js').localStorage;

const Navigate = {
  get: () => localStorage.get("navigate"),
  set: val => localStorage.set("navigate", val),
  remove: () => localStorage.remove("navigate"),
}

module.exports = Navigate;