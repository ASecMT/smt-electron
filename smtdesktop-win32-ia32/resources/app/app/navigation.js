/**
 * Guarda parametros de navegacion
 * para enviar de una pagina a otra
 * 
 * @param {String} url
 * @param {Object} state
 */
function NavigateTo (url, state) {
  _Navigate
    .set(state)
    .then(() => {
      const $a = document.createElement("a");
        $a.href = url
        document.body.appendChild($a);
        $a.click();
    })
}