/**
 * alumno.html
 * - Detalle de alumno
 */

__navigate_promise__.then(nav => window.__navigate__ = nav)
__smtdb_promise__.then(db => {
  window.__smtdb__ = db;
  $("#_smt_username_").html(__smtdb__.user.name);
});

Promise.all([__smtdb_promise__, __navigate_promise__]).then(() => {
  let alumno = __smtdb__.alumnos[__smtdb__.alumnos.findIndex(o => o.IDAlumno == __navigate__.IDAlumno)]; 
  let grupo = __smtdb__.grupos[__smtdb__.grupos.findIndex(g => g.IDGrupo == alumno.IDGrupo)];

  window._alumno = alumno;
  window._grupo = grupo;

  $("#_smt_grupo_materia_").html(`${grupo.Grado}°${grupo.Grupo} ${grupo.Materia}`);
  $("#_smt_nombre_").html(`${alumno.Nombre}`);
  $("#_smt_apellido_paterno_").html(`${alumno.ApellidoPaterno}`);
  $("#_smt_apellido_materno_").html(`${alumno.ApellidoMaterno}`);
  $("#_smt_curp_").html(`${alumno.Curp}`);
  $("#_smt_usaer_").html(`${(alumno.EsUSAER ? "Si" : "No")}`);
});