module.exports = model => `

<tr data-grupo="${model.grupo}" data-tipo="${model.TipoTrabajo}" data-aspecto="" data-portafolio-fecha="${model.Fecha}" data-portafolio-id="${model.IDPortafolio}" data-orden="1" class="resaltar">
    <td rowspan="1">${model.num}</td>
    <td rowspan="1">
        <div class="w100">${model.Nombre}</div> <br />
        <div class="w100">${model.Fecha}</div><br />
        <div class="w100">${model.TipoTrabajo}</div></td>    
    <td data-aspecto=""  colspan="" style="width: 315px;white-space: pre-wrap"></td>
    ${model.tds}
    <td><div data-portafolio-total="${model.IDPortafolio}" class="w100">0</div></td>
    <td ><div data-portafolio-promedio="${model.IDPortafolio}" class="w100">0</div></td>
    <td>
        <textarea name="observacion" class="form-control form-control-oculto" value="${(model.observacion == null || model.observacion == "null" || model.observacion == void 0 ? "" : model.observacion)}" placeholder="Observaciones" style="width:200px">${(model.observacion == null || model.observacion == "null" || model.observacion == void 0 ? "" : model.observacion)}</textarea>
    </td>
</tr>


`