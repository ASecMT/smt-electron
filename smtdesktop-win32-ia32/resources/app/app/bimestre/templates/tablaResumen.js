module.exports = model => `

<table id="tabla-resumen" class="table table-hover table-bordered table-striped">
    <thead>
        <tr>
            <th>Bimestre</th>
            <th>Número De Alumnos</th>
            <th>Alumnos Que Subieron Calificación</th>
            <th>Alumnos Que Bajaron Calificación </th>
            <th>Alumnos Que Bajaron Calificación Y Reprobaron</th>
            <th>Alumnos Que Bajaron Calificación Y Aprobaron </th>
            <th>Alumnos Que Subieron Calificación Y Reprobaron</th>
            <th>Alumnos Que Reprobaron El Bimestre</th>
            <th>Alumnos Que Tienen 6 a 6.9</th>
            <th>Alumnos Que Tienen 7 a 7.9</th>
            <th>Alumnos Que Tienen 8 a 8.9</th>
            <th>Alumnos Que Tienen 9 a 10</th>
            <th>Promedio Bimestral</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>

`