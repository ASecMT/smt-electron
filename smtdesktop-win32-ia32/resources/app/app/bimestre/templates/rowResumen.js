module.exports = model => `

<tr>
    <td>${model.bimestre} Bimestre</td>
    <td>${model.totalAlumnos}</td>
    <td>${model.alumnosSubieron}</td>
    <td>${model.alumnosBajaron}</td>
    <td>${model.alumnosBajaronYReprobaron}</td>
    <td>${model.alumnosBajaronYAprobaron}</td>
    <td>${model.alumnosSubieronYReprobaron}</td>
    <td>${model.alumnosReprobaronBimestre}</td>
    <td>${model.alumnos7}</td>
    <td>${model.alumnos8}</td>
    <td>${model.alumnos9}</td>
    <td>${model.alumnos10}</td>
    <td>${model.promedioBimestral}</td>
</tr>

`