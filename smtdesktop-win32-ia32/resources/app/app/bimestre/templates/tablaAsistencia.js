module.exports = model => `
<table data-tabla="asistencia" class="table table-fixed table-bordered table-hover table-striped text-center icono-violeta" >
    <thead>
        <tr>
            <th rowspan="2"><div class="w50">Sesión</div></th>
            <th rowspan="2"><div class="w150">Fecha</div></th>
            ${model.numerosHeaders}
            <th rowspan="2"><div class="w100">Total</div></th>
            <th rowspan="2"><div class="w100">Promedio</div></th>
            <th rowspan="2"><div class="w200">Observaciones</div></th>
        </tr>
        <tr>
            ${model.nombresHeaders}
        </tr>
    </thead>
    <tbody>
        
    </tbody>
    <tfoot>
        <tr class="loading">
            <td colspan="100" class="text-center">
                <h3><span class="fa fa-spin fa-refresh"></span></h3>
            </td>
        </tr>
        <tr>
            <th rowspan="3"><div style="width:100px">Asistencia</div></th>
            <th><div class="w100">Inasistencias</div></th>
            ${model.rowInacisistencia}
        </tr>
        <tr>
            <th><div class="w100">Asistencias</div></th>
            ${model.rowAsistencia}
        </tr>
    </tfoot>
</table>
`;