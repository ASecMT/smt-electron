module.exports = model => `

<table id="tControl" class="table table-striped table-hover table-bordered table-fixed text-center icono-verde">
    <thead>
        <tr>
            <th><div class="w150  text-center">Alumno</div></th>
            ${model.headers}
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>

`