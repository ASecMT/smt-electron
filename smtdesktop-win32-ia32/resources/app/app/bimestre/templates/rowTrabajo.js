module.exports = model => `

<tr data-grupo="${model.grupo}" data-trabajo-ticks="${model.time}" data-trabajo-fecha="${model.fecha}" data-trabajo-id="${model.id}" class="resaltar">
<td><div class="w50"><a><span data-asitencia-option="eliminar" class="fa fa-trash visor-oculto" title="Eliminar sesión"></span></a>${model.num}</div></td>
    <td><input name="nombre" type="text" class="form-control form-control-oculto" placeholder="Nombre" value="${model.nombre}" style="width:100px" /></td>
    <td><input name="tipo" type="text" class="form-control form-control-oculto" placeholder="Actividad" value="${(model.tipo || "")}" style="width:100px" /></td>
    <td><input name="fecha" type="text" class="form-control form-control-oculto date" placeholder="Fecha de Sesion" value="${model.fecha}" style="width:150px" /></td>
    ${model.tds}
    <td><div data-trabajo-total="${model.id}" class="w100">0</div></td>
    <td ><div data-trabajo-promedio="${model.id}" class="w100">0</div></td>
    <td>
        <textarea name="observacion" class="form-control form-control-oculto" placeholder="Observaciones" style="width:200px">${(model.observacion || "")}</textarea>
    </td>
</tr>

`
