/**
 * pagina de login 
 */

const login = form => new Promise((ok, no) => {
  $.ajax({
    url: __server__ + 'Account/LoginDesktop',
    type: 'POST',
    data: form.serialize(),
    success: response => {
        if (response.result) ok(response)
        else no(response)
    },
    error: () => no({message: "No se pudo conectar al servidor"})
  })
});




const descargarDB = () => new Promise((ok, no) => {
  $.post(__server__ + 'DesktopApi/UserDatabase', res => {
    if (res.result) ok(res)
    else no(res)
  }).fail(() => no({message: "No se pudo conectar al servidor "}))
})

const guardarDB = db => _Logued.set({
    is: true,
    user: {
      id: db.userid,
      name: db.username,
      secciones: db.secciones,
    },
    grupos: db.grupos,
    alumnos: db.alumnos,
    reporte: db.reporte,
    sesiones: db.sesiones,
    trabajos: db.trabajos,
    examenes: db.examenes,
    semaforo: db.semaforo,
    habilidades: db.habilidades,
    instrumentos: db.instrumentos,
    tipoPortafolio: db.tipoPortafolio,
    diagnosticoCiclo: db.diagnosticoCiclo,
  });
 
$('form').submit(function (e) {
    e.preventDefault();

    $(this)
      .find('button')
      .html('Iniciando sesión <span class="fa fa-refresh fa-spin"></span>')
      .attr('disabled', true);

    if ($(this).valid()) {
      let promise = login($(this))
        .then(() => descargarDB()
        .then(resp => guardarDB(resp.data) // guarda la base de datos
        .then(() => _iPCRenderer.send("login")))) // guarda la cookie desde main process
        .catch(res => {
          AlertError(res ? res.message : "No se pudo conectar al servidor")
          $(this)
            .find('button')
            .attr('disabled', false)
            .html('Entrar')
        })
    }
})