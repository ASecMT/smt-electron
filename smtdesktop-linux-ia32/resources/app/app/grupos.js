﻿/**
 * grupos.html
 * - Seccion de grupos, pantalla inicial 
 */

var page = 0;
var buscaOtraVez = true;
var barraActual = 1;

__smtdb_promise__.then(r => {
    window.__smtdb__ = r;
    CargarBarra(barraActual);

    $("#_smt_username_").html(__smtdb__.user.name);
   
});

var CerrarSesion = () => { 
    $.confirm({
        title: 'Desligar cuenta',
        content: "¿Estás seguro que deseas salir? en caso de no haber sincronizado tus datos, se perderán",
        confirmButton: 'SI',
        confirmButtonClass: 'btn-info',
        cancelButton: 'NO',
        icon: 'fa fa-question-circle',
        animation: 'scale',
        confirm: () => {
            window._iPCRenderer.send("logout");
        }
    });
};
var Sincronizar = (silent) => {
     
     
    if(silent !== true) {
          
        Loading("Sincronizando");
      
    }

    console.groupCollapsed(`[${Date.now()}] Attempt to sync`);
    console.info('Sync started');

    _Sync.all($, __smtdb__, __server__).then(smtdb => {
        window.__smtdb__.grupos = smtdb.grupos;
        window.__smtdb__.alumnos = smtdb.alumnos;
        window.__smtdb__.sesiones = smtdb.sesiones;
        window.__smtdb__.trabajos = smtdb.trabajos;
        window.__smtdb__.examenes = smtdb.examenes;
        window.__smtdb__.instrumentos = smtdb.instrumentos;
        window.__smtdb__.habilidades = smtdb.habilidades;
        window.__smtdb__.reporte = smtdb.reporte;
        window.__smtdb__.diagnosticoCiclo = smtdb.diagnosticoCiclo;
        window.__smtdb__.tipoPortafolio = smtdb.tipoPortafolio;
        window.__smtdb__.portafolio = smtdb.portafolio;
        
        CargarBarra(barraActual);
        if(silent !== true) {
            AlertSuccess("Se sincronizó correctamente");
            Loading();
              
        }

        console.info('Successfully synchronized');
        console.groupEnd();
    }).catch(err => {
        if(silent !== true) {
            AlertError((err || {}).message || "Ocurrió un problema");
            Loading();
        }
        
        console.error((err || {}).message || "Error with sync request");
        console.groupEnd();
         
    })
};

let barraGrupo = _require('../app/grupos/templates/barraGrupo.js');
let grupoForm = _require('../app/grupos/templates/grupoForm.js');

function CargarBarra(tipo, mas) {
    var grupos = window.__smtdb__.grupos;

    $('#barraGrupo').empty();
    $('#barraGrupo').append(grupos.filter(g => g.Status == tipo).map(g => barraGrupo(g)));
    $('[data-toggle=tooltip]').tooltip();
}

$(document).ready(function(){
  
   var versionActual = "1.1.9";
  $.ajax({
                      type: 'POST',
                      //determinamos a donde se le va a enviar estos datos.
                      url: "http://mastertools.mx/descargas/version.php",
                      //en data especificamos que le vamos a enviar, creando una variable y con su respectivo valor.
                      data:{}, 
                      //Si la conexión con el servidor fue exitosa, recibirá un msj con el valor del id del usuario que inició sesión.
                      success:function(msj){
                     
                   
                          //si el valor es un numero mayor a 0, entonces inicio sesión con un usuario de la base datos
                                version=msj.version;
                               if(version!=versionActual){
                                 if (confirm("Hay una nueva version ¿desea actulizar?") == true) {
                                       alert("Descargando la nueva versión");
                                        
                                        window.location.href = "http://mastertools.mx/descargas/smt_installer.exe";                         
                                    } else {
                                        
                                    }
                               }

                                    
                               
                      },
                      error:function(){
                          //Si la conexión con el servidor no fue exitosa enviaremos un alert.
                          $('#response').html("Problemas de conexión, intentalo de nuevo");
                      }
                  });             
                                  
                });

function Crear(ID, Tipo) {
    let grupos = window.__smtdb__.grupos;
    let grupo = grupos.filter(g => g.IDGrupo == ID);

    if (Tipo == 1) {
        $("#myModalLabel").html("Talleres");
    } else {
        $("#myModalLabel").html("Grupos");
    }

    let today = new Date();
    let year = today.getFullYear();
    let month = today.getMonth(); 
    let ciclo = month > 6 || (month == 6 && today.getDate() >= 15) ?
        `${year}-${year + 1}` : `${year - 1}-${year}`

    $('#formulario .modal-body').empty();
    $('#formulario .modal-body').append(grupoForm(grupo.length ? grupo[0] : {
        Ciclo: ciclo,
        Color: '',
        Descripcion: '',
        Escuela: '',
        EsTaller: '',
        Grado: '',
        Grupo: '',
        IDGrupo: _uuid.v4(),
        IDUsuario: __smtdb__.user.id,
        Materia: '',
        RegistroFederalEscolar: '',
        Status: 1,
        Turno: ''
    }));

    $('#formulario').modal("show");
}

function Guardar() {
    form = $("#formulario form");
    if (form.valid() == true) {
        var grupos = window.__smtdb__.grupos;
        var serializedForm = form.serializeArray();
        var id = form.find("[name=IDGrupo]").val();
        var grupo = grupos.filter(g => g.IDGrupo == id);

        if (grupo.length) {
            grupo = grupo[0];
        } else {
            grupo = {};
            grupo.Status = 1;
            grupos.push(grupo);
        }

        grupo.FechaSync = (new Date()).toISOString();
        serializedForm.forEach(i => grupo[i.name] = i.value);

        // guardar datos
        _Logued.set(window.__smtdb__);
        // actualizar interfaz
        CargarBarra(barraActual);

        AlertSuccess('Se ha guardado el registro', '');
        $("#formulario").modal("hide");

        // no se q es esto
        //ActualizarMenuLateral();
    }
};

function EliminarGrupo(ID) {
    $.confirm({
        title: 'Grupos',
        content: "¿Estás seguro que deseas eliminar este grupo?",
        confirmButton: 'Aceptar',
        confirmButtonClass: 'btn-info',
        cancelButton: 'Cancelar',
        icon: 'fa fa-question-circle',
        animation: 'scale',
        confirm: function () {
            Loading('Eliminando');
            let grupo = window.__smtdb__.grupos.filter(g => g.IDGrupo == ID);
            if (grupo.length) {
                grupo = grupo[0];
                grupo.Status = 0;
                grupo.FechaSync = (new Date()).toISOString();
                _Logued.set(window.__smtdb__);
                CargarBarra(barraActual);
                // ActualizarMenuLateral();
                Loading();
            }
        }
    });
}

function ArchivarGrupo(ID, archivar) {
    var txt = "¿Estas seguro que deseas " + (archivar == true ? "archivar" : "desarchivar") + " este grupo?";
    $.confirm({
        title: 'Grupos',
        content: txt,
        confirmButton: (archivar == true ? "Archivar" : "Desarchivar"),
        confirmButtonClass: 'btn-info',
        cancelButton: 'Cancelar',
        icon: 'fa fa-question-circle',
        animation: 'scale',
        confirm: () => {
            Loading('Eliminando');
            let grupo = window.__smtdb__.grupos.filter(g => g.IDGrupo == ID);
            if (grupo.length) {
                grupo = grupo[0];
                grupo.Status = archivar == true ? 2 : 1;
                grupo.FechaSync = (new Date()).toISOString();
                _Logued.set(window.__smtdb__);
                AlertSuccess("Se ha archivado el grupo correctamente", "Grupos");
                CargarBarra(barraActual);
                // ActualizarMenuLateral();
                Loading();
            }
        }
    });
}

var clonar = function (id) {
    $.ajax({
        url: '/grupos/clonar',
        type: 'post',
        data: { id: id },
        beforeSend: function () {
            Loading('Clonando grupo');
        },
        complete: function () {
            Loading();
        },
        success: function (response) {
            if (response.result == true) {
                CargarBarra(barraActual);
                Crear(response.data);
                AlertSuccess('Se ha clonado el grupo', 'Grupos');
            }
            else {
                AlertWarning(response.message, 'Grupos');
            }

        }
    });
}

$('#btnBuscar').click(function () {
    barraActual = $('#btnBuscar').text() == 'Activos' ? 1 : 2;
    CargarBarra(barraActual);
    $(this).html($(this).text() == 'Archivados' ? 'Activos' : 'Archivados');
});

function ActualizarMenuLateral() {
    $.ajax({
        type: 'GET',
        url: '/Home/CargarGrupos',
    })
    .done(function (data) {
        $('#menuGrupos').empty(data);
        $('#menuGrupos').append(data);
    });
}