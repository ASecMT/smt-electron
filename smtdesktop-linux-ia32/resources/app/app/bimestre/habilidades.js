var Habilidades = new function () {
    var _a = this;
    var tdsCaptura = '',
        rowNumeros = '',
        rowNombres = '',
        rowInacisistencia = '',
        rowMedios='',
        rowAsistencias = '';

    var generarNombreCache = function () {
        return 'habilidades-' + _grupo + '-' + _bimestre;
    }

    this.listar = function () {
        let habilidades = __smtdb__.habilidades.filter(h => h.IDGrupo == _grupo && h.Bimestre == _bimestre);
        let alumnos = __alumnos__.filter(a => a.Deleted !== true);
        let quitar = () => {
            let items = habilidades.filter(h => alumnos.findIndex(a => a.IDAlumno == h.IDAlumno) == -1);
            items.forEach(i => __smtdb__.habilidades.splice(__smtdb__.habilidades.indexOf(i), 1))
        }
        let agregar = () => {
            alumnos
                .filter(a => !habilidades.filter(h => h.IDAlumno == a.IDAlumno).length)
                .map(a => ({
                    id: _uuid.v4(),
                    Bimestre: _bimestre,
                    IDGrupo: _grupo,
                    IDAlumno: a.IDAlumno,
                    Alumno: [a.ApellidoPaterno, a.ApellidoMaterno, a.Nombre].join(' '),
                    FechaSync: (new Date()).toISOString()
                }))
                .forEach(a => __smtdb__.habilidades.push(a))
        }

        if(habilidades.length != alumnos.length) {
            quitar();
            agregar();

            habilidades = __smtdb__.habilidades.filter(h => h.IDGrupo == _grupo && h.Bimestre == _bimestre);
            return _Logued.set(__smtdb__).then(() => Promise.resolve(habilidades));
        }

        return Promise.resolve(habilidades);
    }

    this.desplegarResultados = function (grupo, selector) {
        // Se generan rows y tds en base a los alumnos
        // Estos html se generaran cada vez que se ejecute desplegar resultados, 
        // una vez generado ya todas las acciones que requieran estos html solo utilizaran el html que se genero

        rowNombres = '';
        rowNumeros = '';
        tdsCaptura = '';
        rowAsistencias = '';
        rowMedios = '';

        rowInacisistencia = '';

        for (var i = 0; i < Alumnos.data.length; i++) {
            var alumn = Alumnos.data[i];
            rowNumeros += `<th>${i + 1}</th>`;
            rowNombres += `<th>${alumn.ApellidoPaterno} ${alumn.ApellidoMaterno} ${alumn.Nombre}</th>`;
            rowInacisistencia += `<td data-alumno-noentrego="${alumn.IDAlumno}">0</th>`;
            rowAsistencias += `<td data-alumno-entrego="${alumn.IDAlumno}">0</th>`;
            rowMedios += `<td data-alumno-medio="${alumn.IDAlumno}">0</th>`;
            tdsCaptura += `<td data-alumno-id="${alumn.IDAlumno}" data-alumno-habilidadid="" data-clase="" data-alumno-habilidad="" data-alumno-habilidades-estado="" tabindex="0"><span class="fa fa-check"></span></td>`;
        }

        let template = _require('../app/bimestre/templates/tablaHabilidades.js');
        var datos = {
            numerosHeaders: rowNumeros,
            nombresHeaders: rowNombres,
            rowAsistencia: rowAsistencias,
            rowMedio: rowMedios,
            rowInacisistencia: rowInacisistencia
        };

        // Se despliega el esqueleto de la asitencias
        $(selector).empty().html(template(datos));


        // Cargar y desplegar datos de sesiones
        _a.listar(grupo).then(function (data) {
            
            _a.generarTrabajo(selector, data, grupo);
            

            $(selector).find('.loading').remove();

            setTimeout(function () {
                // Ligero delay para que se termine de generar las sesiones
                
            }, 500);
        });
    }

    let rowHabilidadesTemplate = _require('../app/bimestre/templates/rowHabilidades.js');

    this.generarTrabajo = function (selector, habilidades, grupo, focus, autoOrdenar) {
        let template = rowHabilidadesTemplate;
        $(selector).find('tbody').empty();
        habilidades.num = $(selector).find('[data-habilidades-id]').length + 1;
        habilidades.tds = tdsCaptura;
        habilidades.grupo = grupo;
        let nombres = ["Se involucra en clase", "Requiere Apoyo Matemáticas ", "Requiere Apoyo Escritura", "Requiere Apoyo Lectura", "Argumentación ", "Síntesis", "Conocimiento", "Coevaluación", "Autoevaluación"];
        
        for (let i = 1; i <= 9; i++) {
            habilidades["grupo"] = grupo;
            habilidades["id"] = i;
            habilidades["nombre"] = nombres[i - 1];
            
            var elementSesion = $(template(habilidades)).prependTo($(selector).find('tbody'));
            var tipo = 0;
            var clase = 0;
            habilidades.map(function (k) {
                var estado = "";
                switch (i) {
                    case 1:
                        estado = k.SeInvolucraClase;
                        tipo = 3;
                        clase = 3;
                        break;
                    case 2:
                        estado = k.ApoyoMatematicas;
                        tipo = 1;
                        clase = 3;
                        break;
                    case 3:
                        estado = k.ApoyoEscritura;
                        tipo = 1;
                        clase = 3;
                        break;
                    case 4:
                        estado = k.ApoyoLectura;
                        tipo = 1;
                        clase = 3;
                        break;
                    case 5:
                        estado = k.Argumentacion;
                        tipo = 2;
                        clase = 2;
                        elementSesion.addClass("separadorHabilidades");
                        break;
                    case 6:
                        estado = k.Sintesis;
                        tipo = 2;
                        clase = 2;
                        break;
                    case 7:
                        estado = k.Conocimiento;
                        tipo = 2;
                        clase = 2;
                        break;                        
                    case 8:
                        estado = k.Coevaluacion;
                        tipo = 1;
                        clase = 1;
                        elementSesion.addClass("separadorHabilidades");
                        break;
                    case 9:
                        estado = k.Autoevaluacin;
                        tipo = 1;
                        clase = 1;
                        elementSesion.addClass("separadorHabilidades");
                        break;                        
                }                    
                if (estado == null || estado==undefined) {
                    estado = "";
                }
                elementSesion.find('[data-alumno-id="' + k.IDAlumno + '"]').attr('data-clase', clase);
                elementSesion.find('[data-alumno-id="' + k.IDAlumno + '"]').attr('data-alumno-habilidad', tipo);
                elementSesion.find('[data-alumno-id="' + k.IDAlumno + '"]').attr('data-alumno-habilidadid', i);
                
                elementSesion.find('[data-alumno-id="' + k.IDAlumno + '"]').attr('data-alumno-habilidades-estado', estado).html(obtenerHtmlEstado(estado, tipo));
                if (i!=1) {
                    actualizarTotales(i);
                }

                if ($('body').hasClass('visualizando'))
                    elementSesion.find('input').attr('disabled',true);
                
            })
        }

        

        // Se actualizan los totales de la sesion y la columna del alumno
        
        ordenar(selector);
        

        if (focus == true) {
            $('html, body').animate({
                scrollTop: elementSesion.offset().top
            }, 1000);

            elementSesion.resaltar('info', 3000)
                            .find('input:first')
                            .focus();
        }

        var contador = 1;
        $(".separadorHabilidades").each(function () {
            if ($(this).next(".separador").length == 0 && $(this).prev(".separador").length == 0) {

                switch (contador) {
                    case 1:
                        var sim = $("<tr class='separador'><td colspan='1000'><h4 class='pull-left'>Evaluación</h4></td></tr>");
                        sim.insertBefore($(this));
                        break;
                    case 2:
                        var sim = $("<tr class='separador'><td colspan='1000'><h4 class='pull-left'>Comprensión Lectora</h4></td></tr>");
                        sim.insertAfter($(this));
                        break;
                    case 3:
                        var sim = $("<tr class='separador'><td colspan='1000'><h4 class='pull-left'>Apoyo</h4></td></tr>");
                        sim.insertAfter($(this));
                        break;
                }
            }
            contador++;
        });
    }

    var ordenar = function (selector) {        

        //var list = $(selector).find('[data-habilidades-fecha]').get();        
        //for (var i = 0; i < list.length; i++) {
        //    list[i].parentNode.appendChild(list[i]);
        //    var hay = $(list[i]).find('td:first').find('span');
        //    if (hay.length == 0) {
        //        //$(list[i]).find('td:first').append('<a><span data-habilidades-option="eliminar" class="fa fa-trash" title="Eliminar sesión"></span></a> ');

        //    }

        //}
    }

    var obtenerHtmlEstado = function (estado, tipo) {
        var estadosResultado = ['<span class="fa fa-close"></span>', '<span class="fa fa-check"></span>', '&frac12'];
        if (estado == null || estado=="") {
            return ' ';
        }
        if (estado==true || estado=='true') {
            return 'Si';
        } else if (estado == false || estado == 'false') {
            return 'No';
        }
        return estado;
    }

    this.mapHabilidad = ["X", "SeInvolucraClase", "ApoyoMatematicas", "ApoyoEscritura", "ApoyoLectura", "Argumentacion", "Sintesis", "Conocimiento", "Coevaluacion", "Autoevaluacin"];
    var actualizarAsistencia = function (alumno, sesion, estado, grupo, habilidad) {
        if (estado == '' || alumno == '' || sesion == '') {
            return;
        }

        // Brincarse inmediatamente al otro elemento sin esperar a que termine
        $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').focusout()
        
        var clase = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').data("clase");
        var cellIndex = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').index()
        switch (clase) {
            case 1:
                var nxt = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').closest('tr').next().children().eq(cellIndex);
                var nxtClase = nxt.data("clase");
                if (nxtClase==1) {
                    nxt.focus();
                }
                else if ($('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').next('td[tabindex]').length > 0) {
                    nxt = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').closest('tr').prev().children().eq(cellIndex + 1);
                    nxt.focus();
                }
                else {
                    nxt = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').closest('tr').next().next().children().eq(1)
                    nxt.focus();
                }
                break;
            case 2:
                var nxt = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').closest('tr').next().children().eq(cellIndex);
                var nxtClase = nxt.data("clase");
                if (nxtClase == 2) {
                    nxt.focus();

                }
                else if ($('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').next('td[tabindex]').length > 0) {
                    nxt = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').closest('tr').prev().prev().children().eq(cellIndex + 1);
                    nxt.focus();
                }
                else {
                    nxt = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').closest('tr').next().next().children().eq(1)
                    nxt.focus();
                }
                break;
            case 3:
                var nxt = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').closest('tr').next().children().eq(cellIndex);
                var nxtClase = nxt.data("clase");
                if (nxtClase == 3) {
                    nxt.focus();
                } else {
                    nxt = $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]').closest('tr').prev().prev().prev().children().eq(cellIndex + 1);
                    nxt.focus();
                }
                break;
        
        }

        let predicate = h => h.IDAlumno == alumno && h.IDGrupo == _grupo && h.Bimestre == _bimestre;
        let hab = __smtdb__.habilidades.filter(predicate);
        if (hab.length) {
            let habName = _a.mapHabilidad[+habilidad];
            hab[0][habName] = habName == "SeInvolucraClase" ? 
                estado == "Si" ? true : false : 
                /^\d+$/.test(estado) ? +estado : estado;
            hab[0].FechaSync = (new Date()).toISOString();
            _Logued.set(__smtdb__).then(() => {
                // Se despliega la sesion actualizada con efecto
                $('[data-alumno-habilidadid="' + habilidad + '"][data-alumno-id="' + alumno + '"]')
                    .attr('data-alumno-habilidades-estado', estado)
                    .html(obtenerHtmlEstado(estado))
                    .resaltar('info', 800);

                actualizarTotales(habilidad);
            })
        }
    }

    var actualizaDataEnCache = function () {
        // Se complica actualizar los datos, asi que se elimina el cache y la proxima vez se volvera a descargar todo
        Cache.vaciar(generarNombreCache());
    }
   
    var actualizarTotales = function (sesion) {
        var selector = $('[data-tabla="habilidades"]');
        if (sesion != 1) {
            var totalSesionAsistencia = $(selector).find('[data-alumno-habilidadid="' + sesion + '"]').length;
            var total = 0
            $(selector).find('[data-alumno-habilidadid="' + sesion + '"]').each(function () {
                var val = $(this).attr("data-alumno-habilidadid-estado");
                if (val != null) {
                    total += parseInt(val);    
                }
            });
            var promedio = (((total) / (totalSesionAsistencia * 10)) * 100).toFixed(2);
            
            $(selector).find('[data-habilidades-total="' + sesion + '"]').html(total>0?total:0);
            $(selector).find('[data-habilidades-promedio="' + sesion + '"]').html(promedio != "NaN" ? promedio + "%" : 0);
        }
    }   

    this.capturaHabilidades1 = _require('../app/bimestre/templates/capturaHabilidades1.js');
    this.capturaHabilidades2 = _require('../app/bimestre/templates/capturaHabilidades2.js');
    this.capturaHabilidades3 = _require('../app/bimestre/templates/capturaHabilidades3.js');

    // Generar select para la captura
    $('body:not(.visualizando)').delegate('[data-alumno-id][data-alumno-habilidades-estado]', 'focusin', function () {
        var td = $(this);
        var template = '';        
        if (td.find('select').length == 0) {
            switch ($(this).attr("data-alumno-habilidad")) {
                case "1":
                    template = _a.capturaHabilidades1();
                    break;
                case "2":
                    template = _a.capturaHabilidades2();
                    break;
                case "3":
                    template = _a.capturaHabilidades3();
                    break;        
            }

            td.empty();
            $(template).appendTo(td)
                .focus(function () {
                    // Esto hace que se carge el select abierto.. no encontre otra manera
                    $(this).attr('size', $(this).attr("expandto"));
                })
                .focus();
        }
    });

    // Reestablecer el td para mostrar unicamente el texto del estado
    $('body:not(.visualizando)').delegate('[data-alumno-id][data-alumno-habilidades-estado] select', 'focusout', function () {

        var estado = $(this).parents('td').attr('data-alumno-habilidades-estado');
        $(this).parents('td').html(obtenerHtmlEstado(estado));
    });

    // Cambiar el estado inmediatamente al seleccionar opcion
    $('body:not(.visualizando)').delegate('[data-alumno-id][data-alumno-habilidades-estado] select', 'change', function () {

        if ($(this).val() != '') {
            var alumno = $(this).parents('td').attr('data-alumno-id');
            var sesion = $(this).parents('tr').attr('data-habilidades-id');
            var grupo = $(this).parents('tr').attr('data-grupo');
            var habilidad = $(this).parents('td').attr('data-alumno-habilidadid');
            actualizarAsistencia(alumno, sesion, $(this).val(), grupo, habilidad);
        }
        else
            $(this).focusout();
    });

    $('body:not(.visualizando)').delegate('[data-habilidades-id] [name="observacion"],[data-habilidades-id] [name="fecha"],[data-habilidades-id] [name="nombre"]', 'change', function () {
        var tr = $(this).parents('[data-habilidades-id]');

        var fecha = tr.find('[name="fecha"]').val();
        if (tr.find('[name="fecha"]').attr('type') != 'text') {
            fecha = fecha.substr(8, 2) + '-' +
                    fecha.substr(5, 2) + '-' +
                    fecha.substr(0, 4);
        }

        _a.editar(tr.attr('data-habilidades-id'), fecha, tr.find('[name="observacion"]').val(), tr.find('[name="nombre"]').val());
    });

    // Convertir el text a date
    $('body:not(.visualizando)').delegate('[data-habilidades-id] [name="fecha"]', 'focusin', function () {
        if ($(this).attr('type') != 'date') {
            var valor = this.value;

            valor = valor.substr(6, 4) + '-' +
                    valor.substr(3, 2) + '-' +
                    valor.substr(0, 2);

            $(this).val(valor).attr('type', 'date').css('width', 160);
        }
    });

    // Convertir el date a text
    $('body:not(.visualizando)').delegate('[data-habilidades-id] [name="fecha"]', 'focusout', function () {
        if ($(this).attr('type') != 'text') {
            var valor = this.value;

            valor = valor.substr(8, 2) + '-' +
                    valor.substr(5, 2) + '-' +
                    valor.substr(0, 4);

            $(this).attr('type', 'text').val(valor).css('width', 100);
        }
    });

    $('body:not(.visualizando)').delegate('[data-habilidades-id] [data-habilidades-option="eliminar"]', 'click', function () {
        var tr = $(this).parents('[data-habilidades-id]');
        _a.eliminar(tr.attr('data-habilidades-id'));
    });
    $('body:not(.visualizando)').delegate('[data-habilidades-id] [data-habilidades-option="editardescripcion"]', 'click', function () {
        var tr = $(this).parents('[data-habilidades-id]');
        _a.modaleditar(tr.attr('data-habilidades-id'));
    });

    this.Imprimir = function () {
        let url = __server__ + '/habilidades/imprimir?grupo=' + _grupo + '&bimestre=' + _bimestre;
        let download = _Downloader.download(url);
        Loading("...")
        download.then(res => {
            Loading();
            open(res.file); 
        }).catch(err => {
            console.error(err)

            _Downloader.cached(url).then(res => {
                Loading();
                open(res.file);
                AlertError("Mostrando versión desactualizada, conectese a internet para ver una versión actualizada")
            }).catch(err => {
                AlertError("Conectese a internet para descargar la version para imprimir")
                Loading();
                console.error(err)
            })
        })
    }
}

$('body:not(.visualizando)').delegate('[data-habilidades="nuevo"]', 'click', function () {
    Trabajo.nuevo('#tabla-habilidades', 1);
});

$('body:not(.visualizando)').delegate('[data-habilidades="suspencion"]', 'click', function () {
    Trabajo.nuevo('#tabla-trabajo', 3);
});