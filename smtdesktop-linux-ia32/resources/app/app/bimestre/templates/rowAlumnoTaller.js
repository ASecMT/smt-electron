module.exports = model => `
<tr data-alumno-id="${model.IDAlumno}" data-alumno-nombre="${model.ApellidoPaterno} ${model.ApellidoMaterno} ${model.Nombre}" class="${model.Color}" style="background-color:${model.Color}">    
    <td>${model.ApellidoPaterno}</td>
    <td>${model.ApellidoMaterno}</td>
    <td>${model.Nombre}</td>
    <td>${model.Grupo}</td>
    <td class="text-center">
        <div class="btn-group">
            <button data-alumno="detalles" onclick="abrirDetalle('${model.IDAlumno}')" type="button" class="btn btn-default" style="width:100px">Ver</button>
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu visor-oculto">
                <li><a href="#" data-alumno="editar" onclick="Alumnos.cargarAlumno('${model.IDAlumno}')">Editar</a></li>
                <li><a href="#" data-alumno="eliminar" onclick="Alumnos.eliminarAlumno('${model.IDAlumno}')">Eliminar</a></li>
            </ul>
        </div>
    </td>
</tr>`