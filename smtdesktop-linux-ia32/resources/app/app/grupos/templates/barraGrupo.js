module.exports = model => `
    <div class="col-md-3 text-white">
        <div class="panel" style="background-color:${model.Color || "#ebebeb"}">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <div class="huge ${model.Color == "#ebebeb" || model.Color == null ? "text-black" : ""}">${model.Grado}${model.Grupo}</div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="text-medium text-overflow ${model.Color == "#ebebeb" || model.Color == null ? "text-black" : ""}">${model.Materia}</div>
                        <div class="text-overflow ${model.Color == "#ebebeb" || model.Color == null ? "text-black" : ""}">${model.Escuela} (${model.Ciclo})</div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <span data-toggle="tooltip" data-placement="top" title="Bimestre1">
                    <a class="btn btn-default btn-circle" href="javascript:NavigateTo('bimestre.html', { IDGrupo: '${model.IDGrupo}', Bimestre: 1 })">1</a>
                </span>
                <span data-toggle="tooltip" data-placement="top" title="Bimestre2">
                    <a class="btn btn-default btn-circle" href="javascript:NavigateTo('bimestre.html', { IDGrupo: '${model.IDGrupo}', Bimestre: 2 })">2</a>
                </span>
                <span data-toggle="tooltip" data-placement="top" title="Bimestre3">
                    <a class="btn btn-default btn-circle" href="javascript:NavigateTo('bimestre.html', { IDGrupo: '${model.IDGrupo}', Bimestre: 3 })">3</a>
                </span>
                <span data-toggle="tooltip" data-placement="top" title="Bimestre4">
                    <a class="btn btn-default btn-circle" href="javascript:NavigateTo('bimestre.html', { IDGrupo: '${model.IDGrupo}', Bimestre: 4 })">4</a>
                </span>
                <span data-toggle="tooltip" data-placement="top" title="Bimestre5">
                    <a class="btn btn-default btn-circle" href="javascript:NavigateTo('bimestre.html', { IDGrupo: '${model.IDGrupo}', Bimestre: 5 })">5</a>
                </span>
                    <div class="dropdown pull-right visor-oculto" data-toggle="tooltip" data-placement="top" title="Configuracion">
                        <a id="btn${model.IDGrupo}" href="javascript.void(0);" class="btn text-black" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-cog"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="btn${model.IDGrupo}">
                            <li><a href="#" onclick="Crear('${model.IDGrupo}')">Configurar</a></li>
                            <!--li><a href="#" onclick="clonar('${model.IDGrupo}')">Clonar</a></li-->
                            ${(model.Status != 2 ?
                                `<li><a href="#" onclick="ArchivarGrupo('${model.IDGrupo}',true)">Archivar</a></li>` :
                                `<li><a href="#" onclick="ArchivarGrupo('${model.IDGrupo}',false)">Desarchivar</a></li>`
                            )}
                            <li><a href="#" onclick="EliminarGrupo('${model.IDGrupo}')">Eliminar</a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
            </div>
        </div>
    </div>`