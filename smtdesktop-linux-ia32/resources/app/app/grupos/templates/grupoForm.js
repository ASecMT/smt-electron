module.exports = model => `
<script src="../scripts/jquery.validate.min.js"></script>
<script src="../Scripts/jquery.validate.unobtrusive.min.js"></script>

<form method="post">
    <input data-val="true" data-val-number="The field IDGrupo must be a number." data-val-required="El campo IDGrupo es obligatorio." id="IDGrupo" name="IDGrupo" type="hidden" value="${model.IDGrupo}" />
    <input id="EsTaller" name="EsTaller" type="hidden" value="${model.EsTaller}" />
    <div class="col-md-12 col-sm-12">
        <div class="col-md-6 col-xs-6 form-group">
            <label for="Materia">Materia</label><br />
            <input class="form-control col-12" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-required="Es obligatorio" id="Materia" name="Materia" required="true" type="text" value="${model.Materia}" /><br />
            <span class="field-validation-valid" data-valmsg-for="Materia" data-valmsg-replace="true"></span>
        </div>
        <div class="col-md-6 col-xs-6 form-group" style="min-height:80px;">
            <label for="Grado">Grado</label><br />
            <select name="Grado" id="Grado" class="form-control" required="required">
                <option value="">Seleccione</option>
                <option value="1" ${model.Grado == 1 ? "selected" : ""}>1</option>
                <option value="2" ${model.Grado == 2 ? "selected" : ""} >2</option>
                <option value="3" ${model.Grado == 3 ? "selected" : ""} >3</option>
                <option value="4" ${model.Grado == 4 ? "selected" : ""} >4</option>
                <option value="5" ${model.Grado == 5 ? "selected" : ""} >5</option>
                <option value="6" ${model.Grado == 6 ? "selected" : ""} >6</option>
            </select>
            <span class="field-validation-valid" data-valmsg-for="Grado" data-valmsg-replace="true"></span>
        </div>

        <div class="col-md-6 col-xs-6 form-group">
            <label for="Grupo">Grupo</label><br />
            <input class="form-control col-12" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-regex="Formato Incorrecto (Ej. ABC, A, B,°)" data-val-regex-pattern="^[A-Za-z]{1,3}|°" data-val-required="Es obligatorio" id="Grupo" name="Grupo" required="true" type="text" value="${model.Grupo}">
            <span class="field-validation-valid" data-valmsg-for="Grupo" data-valmsg-replace="true"></span>
        </div>

        <div class="col-md-6 col-xs-6 form-group">
            <label for="Escuela">Escuela</label><br />
            <input class="form-control col-12" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-required="Es obligatorio" id="Escuela" name="Escuela" required="true" type="text" value="${model.Escuela}" /><br />
            <span class="field-validation-valid" data-valmsg-for="Escuela" data-valmsg-replace="true"></span>
        </div>
        <div class="col-md-6 col-xs-6 form-group">
            <label for="RegistroFederalEscolar">Registro Federal Escolar</label><br />
            <input class="form-control col-12" data-val="true" data-val-length="La longitud máxima de caracteres permitidos es de 150" data-val-length-max="150" data-val-required="Es obligatorio" id="RegistroFederalEscolar" name="RegistroFederalEscolar" required="true" type="text" value="${model.RegistroFederalEscolar}" /><br />
            <span class="field-validation-valid" data-valmsg-for="RegistroFederalEscolar" data-valmsg-replace="true"></span>
        </div>
        <div class="col-md-6 col-xs-6 form-group"  style="min-height:80px;">
            <label for="Turno">Turno</label><br />
            <select name="Turno" id="Turno" class="form-control" required="required">
                <option value="">Seleccione</option>
                <option value="Matutino" ${model.Turno == "Matutino" ? "selected" : ""}>Matutino</option>
                <option value="Vespertino" ${model.Turno == "Vespertino" ? "selected" : ""} >Vespertino</option>
                <option value="Nocturno" ${model.Turno == "Nocturno" ? "selected" : ""}>Nocturno</option>
            </select>
            <span class="field-validation-valid" data-valmsg-for="Turno" data-valmsg-replace="true"></span>
        </div>
        <div class="col-md-6 col-xs-6 form-group">
            <label for="Ciclo">Ciclo</label><br />
            <input class="form-control col-12" data-val="true" data-val-regex="Formato Incorrecto (Ej. 2015-2016)" data-val-regex-pattern="^[0-9]{4}[\\\-]{1}[0-9]{4}$" data-val-required="Es obligatorio" id="Ciclo" name="Ciclo" required="true" type="text" value="${model.Ciclo}" /><br />
            <span class="field-validation-valid" data-valmsg-for="Ciclo" data-valmsg-replace="true"></span>
        </div>
        <div class="col-md-6 col-xs-6 form-group">
            <label for="Color">Color de Fondo</label><br />
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default ">
                    <input type="radio" name="Color" ${model.Color == "#ebebeb" ? "checked" : ""} value="#ebebeb"  autocomplete="off" > 1
                </label>
                <label class="btn btn-success">
                    <input type="radio" name="Color" ${model.Color == "#5cb85c" ? "checked" : ""} value="#5cb85c"  autocomplete="off"> 2
                </label>
                <label class="btn btn-primary">
                    <input type="radio" name="Color" ${model.Color == "#337ab7" ? "checked" : ""} value="#337ab7"  autocomplete="off"> 3
                </label>
                <label class="btn btn-warning">
                    <input type="radio" name="Color" ${model.Color == "#f0ad4e" ? "checked" : ""} value="#f0ad4e"  autocomplete="off"> 4
                </label>
                <label class="btn btn-danger">
                    <input type="radio" name="Color" ${model.Color == "#d9534f" ? "checked" : ""} value="#d9534f" autocomplete="off"> 5
                </label>
                <label class="btn btn-info">
                    <input type="radio" name="Color" ${model.Color == "#5bc0de" ? "checked" : ""} value="#5bc0de" autocomplete="off"> 6
                </label>
            </div>
        </div>
    </div>
</form>
`