/**
 * Buscador de alumnos en barra superior 
 */

var substringMatcher = function(strs) {
  return function findMatches(q, callback) {
    var matches, substringRegex;

    matches = [];
    substrRegex = new RegExp(q, 'i');

    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    callback(matches);
  };
};

const setupSearchInput = () => {
    let $input = $('.typeahead');

    $input.typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'states',
        limit: 15,
        source: substringMatcher(__smtdb__.alumnos.map(a => `${a.ApellidoPaterno} ${a.ApellidoMaterno} ${a.Nombre}`))
    });

    $input.on('typeahead:select', (e, v) => {
        let index = __smtdb__.alumnos.findIndex(a => `${a.ApellidoPaterno} ${a.ApellidoMaterno} ${a.Nombre}` === v);
        let alumno = __smtdb__.alumnos[index]
        
        NavigateTo('alumno.html', { IDAlumno: alumno.IDAlumno })
    })    
};

$(() => {
    __smtdb_promise__.then(() => setupSearchInput());
});